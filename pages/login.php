<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../adminku/assets/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../adminku/assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="../adminku/assets/assets/css/style.css" rel="stylesheet">
    <link href="../adminku/assets/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" method="POST" action="../script/aut_login.php">
		        <h2 class="form-login-heading">sign in now</h2>
		        <div class="login-wrap">
		            <input type="text" name='username' class="form-control" placeholder="Username" autofocus>
		            <br>
		            <input type="password" name="password" class="form-control" minlength="3" maxlength="10" placeholder="Password">
		            <br/>
		            <button class="btn btn-theme btn-block" name="submit" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		            <hr>
                <br>
                <center><a class="nav-link js-scroll-trigger" href="../index.php">Kembali ke halaman utama</a></center>
		            
		        </div>
		
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../adminku/assets/assets/js/jquery.js"></script>
    <script src="../adminku/assets/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="../adminku/assets/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("../adminku/assets/assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
