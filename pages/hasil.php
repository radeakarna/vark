
    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in"> <p>Penentuan Modalitas anda sudah berhasil!</p></div>
        </div>
      </div>
    </header>

    <!-- About -->
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Hasil Penentuan Modalitas Anda</h2><br/>
              <!--<h3 class="section-subheading text-muted">pilihlah jawaban sesuai dengan kebiasaanmu</h3>-->
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12"><center>
            <table width="80%" >
              <tr>
                <td width="100%">
                    <?php
include "script/koneksi.php";
$data_diri = $_SESSION['nim'];
$query     = mysqli_query($con, "SELECT *FROM biodata_pengunjung WHERE nim = '$data_diri' ");
$r         = mysqli_fetch_array($query);

?>
                    <table cellpadding="7" width="100%" border="1">
                      <tr>
                        <td><label>NIM</label></td><td><label>:</label></td><td><label for="exampleInputEmail1"><?php echo $r['nim']; ?></label><br/></td>
                      </tr>
                      <tr>
                        <td><label>Nama</label></td><td><label>:</label></td><td><label for="exampleInputEmail1"><?php echo $r['nama_pengunjung']; ?></label><br/></td>
                      </tr>
                      <tr>
                        <td><label>Jenis Kelamin</label></td><td><label>:</label></td><td><label for="exampleInputEmail1"><?php echo $r['jns_kelamin']; ?></label><br/></td>
                      </tr>
                      <tr>
                        <td><label>Jurusan</label></td><td><label>:</label></td><td><label for="exampleInputEmail1"><?php echo $r['jurusan']; ?></label><br/></td>
                      </tr>
                      <tr>
                        <td><label>Poin</label></td><td><label>:</label></td><td>
                        <?php
$nilai  = mysqli_query($con, "SELECT *FROM hasil WHERE nim = '$data_diri' ");
$rnilai = mysqli_fetch_array($nilai);
?>
                          <label><button class="btn btn-primary">Visual = <?php echo $rnilai['jml_visual']; ?></button></label>
                          <label><button class="btn btn-info">Auditorial = <?php echo $rnilai['jml_auditorial']; ?></button></label>
                          <label><button class="btn btn-success">Readwrite = <?php echo $rnilai['jml_readwrite']; ?></button></label>
                          <label><button class="btn btn-danger">Kinestetik = <?php echo $rnilai['jml_kinestetik']; ?></button></label>
                        </td>
                      </tr>
                      <tr>
                        <td><label>Kecenderungan</label></td><td><label>:</label></td><td><label for="exampleInputEmail1">
                          <?php
$data          = mysqli_query($con, "SELECT *FROM hasil WHERE nim = '$data_diri' ");
$rr            = mysqli_fetch_array($data);
$kecenderungan = $rr['kecenderungan'];
$arr_kalimat   = explode("-", $kecenderungan);
?>
                          <?php
$data_warna = array('Visual' => 'btn-primary', 'Auditorial' => 'btn-info', 'Read/Write' => 'btn-success', 'Kinestetik' => 'btn-danger');
foreach ($arr_kalimat as $value) {
    echo '<a href="index.php?p=vhasil&modalitas=' . $value . '"><button type="button" name="lihat_hasil" id="lihat_hasil" class="btn ' . $data_warna[$value] . '">' . $value . '</button></a> ';
    //echo "<br />";
}
?>
                        </td>
                      </tr>
                    </table>

                </td>

              </tr>

            </table>
            </center>

          </div>
        </div>
      </div>
    </section>



