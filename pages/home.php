<!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Ingin Tahu Cara Belajar Terbaikmu ?</div>
          <div class="intro-heading text-lowercase"> Cek Yuk!</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="tes.php" data-toggle="modal" data-target="#myModal">Mulai!</a>
        </div>
      </div>
    </header>

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" style="background-color: black;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Form Biodata</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>
            <form action="script/simpan_pengunjung.php" method="post" enctype="multipart/form-data">
              <div class="modal-body">

                  <div class="form-group">
                    <label class="control-label" for="jns_modalitas">NIM</label>
                    <input type="text" name="nim" class="form-control" id="jns_modalitas" required>
                </div>

                <div class="form-group">
                    <label class="control-label" for="des_modalitas">Nama</label>
                    <input type="text" name="nama_pengunjung" class="form-control" id="des_modalitas" required>
                </div>

                  <div class="form-group">
                    <label class="control-label" for="jns_kelamin">Jenis Kelamin</label>
                    <select class="form-control" id="tampil_disini" name="jns_kelamin">
                      <option value="laki-laki">Laki - Laki</option>
                        <option value="perempuan">Perempuan</option>
                    </select>
                </div>

                  <div class="form-group">
                    <label class="control-label" for="des_modalitas">Jurusan</label>
                    <select name="jurusan" class="form-control" id="des_modalitas">
                    <option value="Informatika">Informatika</option>
                    <!-- <option value="Komunikasi">Komunikasi</option> -->
                    <option value="Pendidikan teknik informatika">Pendidikan teknik informatika</option>
                    </select>
                </div>
              </div>
          </p>
        </div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-danger">Reset</button>
          <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio" >
    <!--style="background-image: url(img/header-bg.jpg);" -->
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Profil Modalitas Belajar VARK</h2>
            <h3 class="section-subheading text-muted">Kamu bisa belajar tentang modalitas V-A-R-K disini!</h3>
          </div>
        </div>
        <div class="row">
          <table width="100%" border="1" cellpadding="5" cellspacing="3">
            <tr style="background-color: skyblue;">
              <th width="5%">
                No
              </th>
              <th width="20%">
                Jenis Modalitas
              </th>
              <th>
                Karakter
              </th>
            </tr>
            <?php
include "script/koneksi.php";
$no   = 1;
$data = mysqli_query($con, "SELECT *FROM profil");
while ($r = mysqli_fetch_assoc($data)) {
    ?>

            <tr>
              <td align="center">
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $r['jns_modalitas']; ?>
              </td>
              <td align="justify">
                <?php echo $r['karakter']; ?>
              </td>
            </tr>
            <?php
$no++;}
?>
          </table>

        </div>
      </div>
    </section>



