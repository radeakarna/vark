<?php
session_start();
if (!isset($_SESSION['nim'])) {
    echo "<script>alert('Silahkan mendaftar terlebih dahulu');window.location='index.php';</script>";   
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kelas | VARK</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- Icheck -->
        <link href="assets/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .soal {
              margin: 10px 10px 20px 20px;
            }

            .test {
              margin: 10px 10px 20px 20px;
            }/*
            html {
              scroll-behavior: smooth;
            }*/
        </style>
    </head>
    <body class="skin-black fixed" data-spy="scroll" data-target="#scrollspy">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Kelas <?= $_GET['modal']; ?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="assets/index.html">
                                <i class="fa fa-dashboard"></i> <span>Home</span>
                            </a>
                        </li>
                        <?php
                        include "../script/koneksi.php";
                        $data_diri = $_SESSION['nim'];
                        $getModal  = $_GET['modal'];
                        $no        = 1;
                        $getVark = $con->query("SELECT id_solusi FROM solusi WHERE jns_modalitas = '$getModal'");
                        $gtr = mysqli_fetch_assoc($getVark);
                        //$query     = mysqli_query($con, "SELECT materi.materi_id,materi.materi FROM materi INNER JOIN sub_materi ON materi.materi_id = sub_materi.materi_id INNER JOIN solusi ON sub_materi.id_solusi = solusi.id_solusi WHERE solusi.jns_modalitas ='$getModal' GROUP BY materi.materi");
                        $query = mysqli_query($con, "SELECT materi_id,materi FROM materi GROUP BY materi.materi ORDER BY materi_id");
                        while ($r = mysqli_fetch_assoc($query)) {
                        ?>
                        <li class="treeview">
                            <a href="<?php echo '#' . $r['materi_id']; ?>">
                                <i class="fa fa-list"></i> <span><?php echo $r['materi']; ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php
                                    //Menampilkan materi sub bab
                                    $solusi = $gtr['id_solusi'];
                                    $mainMateri = mysqli_query($con, "SELECT *FROM sub_materi WHERE materi_id = '$r[materi_id]' AND id_solusi = '$solusi' ");
                                    while ($main = mysqli_fetch_assoc($mainMateri)) {
                                ?>
                                    <li><a href="<?php echo '#sub' . $main['sub_materi_id']; ?>" style="font-size: 12px !important;"> <?=$main['nama_materi']?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php
                        $no++;}
                        ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <?php
                if (isset($_GET['modal'])) {
                    $page = $_GET['modal'];
                    switch ($page) {
                        case 'Visual':
                            include "halaman/visual.php";
                            break;
                        case 'Read/Write':
                            include "halaman/rw.php";
                            break;
                        case 'Auditorial':
                            include "halaman/auditorial.php";
                            break;
                        case 'Kinestetik':
                            include "halaman/kinestetik.php";
                            break;
                        default:
                            echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
                            break;
                    }
                } else {
                    include "halaman/home.php";
                }
                ?>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- Icheck -->
        <script src="assets/js/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- Slim scrool -->
        <script src="assets/js/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            /** add active class and stay opened when selected */
            var url = window.location;

            // for sidebar menu entirely but not cover treeview
            $('ul.sidebar-menu a').filter(function() {
                 return this.href == url;
            }).parent().addClass('active');

            // for treeview
            $('ul.treeview-menu a').filter(function() {
                 return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
        </script>

        <script type="text/javascript">
            // Select all links with hashes
            $('a[href*="#"]')
              // Remove links that don't actually link to anything
              .not('[href="#"]')
              .not('[href="#0"]')
              .click(function(event) {
                // On-page links
                if (
                  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
                  && 
                  location.hostname == this.hostname
                ) {
                  // Figure out element to scroll to
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                  // Does a scroll target exist?
                  if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 1000, function() {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                      } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                      };
                    });
                  }
                }
              });
        </script>

<script src="assets/js//jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#dragdiv span").draggable({
            appendTo: "body",
            helper: "clone",
            cursor: "move",
            revert: "invalid"
        });

        initDroppable($(".dropdiv"));
        function initDroppable($elements) {
            $elements.droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-drop-hover",
                accept: ":not(.ui-sortable-helper)",

                over: function(event, ui) {
                    var $this = $(this);
                },
                drop: function(event, ui) {
                    var $this = $(this);
                    if ($this.val() == '') {
                        $this.val(ui.draggable.text());
                    } else {
                        $this.val(ui.draggable.text());
                        //$this.val($this.val() + "," + ui.draggable.text());
                    }
                }
            });
        }
    });
</script>
<script type="text/javascript">
    function postNote (name) {
        var formData = new FormData($('#'+name)[0]);
        //console.log(formData);
        $.ajax({
          url: "script/postNote.php",
          data : formData,
          type:"post",
          contentType: false,
          processData: false,
          dataType: "JSON",
          beforeSend:function(){
            //$('#ayogaskan').text("proses...");
          },
          success:function(data){
            //$('#ayogaskan').text("kirim");
            if (data.status == 'update') {
                alert('Catatan ter update');
            }else{
                alert('Catatan berhasil di simpan');
            }
            $('#'+name).val(data.note);
                  
          },
          error: function(jqXHR, textStatus, errorThrown){
            console.log(errorThrown);
            //$('#ayogaskan').text("kirim");
            //swal("Error update data!", "Please try again", "error");
          }
        });
    }
</script>

    </body>
</html>