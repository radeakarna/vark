<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kelas
        <small><?=$getModal;?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
    </ol>
</section>

<!-- Main content -->
<?php
//Menampilkan materi perbab
$getMateri = mysqli_query($con, "SELECT sub_materi.materi_id,sub_materi.nama_materi,sub_materi.sub_materi_id, materi.materi FROM materi INNER JOIN sub_materi ON materi.materi_id = sub_materi.materi_id INNER JOIN solusi ON sub_materi.id_solusi = solusi.id_solusi WHERE solusi.jns_modalitas ='" . $_GET['modal'] . "' ORDER BY materi_id ");
while ($materi = mysqli_fetch_assoc($getMateri)) {
?>
<section class="content" id="<?php echo $materi['materi_id']; ?>">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable"> 
            <!-- Box (with bar chart) -->
            <div class="box box-danger" id="loading-example">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                       
                    </div><!-- /. tools -->
                    <i class="fa fa-list"></i>

                    <h3 class="box-title"><?= $materi['materi']; ?></h3>
                </div><!-- /.box-header no-padding -->
                <div class="box-body ">
                    <?php
                        //Menampilkan materi sub bab
                        $mainMateri = mysqli_query($con, "SELECT *FROM sub_materi WHERE materi_id = '$materi[materi_id]' AND id_solusi = '3' ");
                        while ($main = mysqli_fetch_assoc($mainMateri)) {
                    ?>
                        <!-- Box (with bar chart) -->
                        <div class="box box-aqua" id="sub<?=$main['sub_materi_id']; ?>" style="box-shadow: none;border-radius: 0px;">
                            <div class="box-header">
                                <h3 class="box-title"><?= $main['nama_materi']; ?></h3>
                            </div>
                            <div class="box-body">
                                <div class="well"><?= $main['keterangan']; ?></div>
                                <ol type="A">
                                    <?php
                                    $getMainMateri = mysqli_query($con, "SELECT *FROM main_materi WHERE sub_materi_id = '$materi[sub_materi_id]' ORDER BY no_urut");
                                    while ($gm = mysqli_fetch_assoc($getMainMateri)) {
                                    //main materi
                                    ?>
                                    <h3 class="mb-0"><li><?=$gm['materi']?></li></h3><br/>
                                    <p><a href="hipermeida.php?materi=<?=$materi['materi_id']?>#video"><span class="btn btn-danger btn-flat">Video</span></a> | <a href="hipermeida.php?materi=<?=$materi['materi_id']?>#text"><span class="btn btn-info btn-flat">Text</span></a> | <a href="hipermeida.php?materi=<?=$materi['materi_id']?>#gambar"><span class="btn btn-warning btn-flat">Audio</span></a> </p>
                                    <p class="lead mb-5">
                                        <audio controls>
                                          <source src="audio/<?=$gm['isi_materi'];?>" type="audio/mpeg" controls>
                                        </audio>
                                    </p>
                                    <p><a href="hipermeida.php?materi=<?=$materi['materi_id']?>#video"><span class="btn btn-danger btn-flat">Video</span></a> | <a href="hipermeida.php?materi=<?=$materi['materi_id']?>#text"><span class="btn btn-info btn-flat">Text</span></a> | <a href="hipermeida.php?materi=<?=$materi['materi_id']?>#gambar"><span class="btn btn-warning btn-flat">Audio</span></a> </p>
                                    <?php } mysqli_free_result($getMainMateri); ?>
                                </ol>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <?php 
                                $cekTest = $con->query("SELECT benar FROM test WHERE sub_materi_id = '" . $materi['sub_materi_id'] . "' AND nim = '$data_diri'");
                                if (mysqli_num_rows($cekTest) == 0) {
                                ?>
                                  <div class="soal">
                                    <h3>Soal - soal</h3>
                                    <form method="post" action="script/podtTest.php?sub_materi=<?=$materi['sub_materi_id']?>">
                                    <?php
                                    $nomorsoal = 1;
                                    $getSoal   = $con->query("SELECT id, submateri_id, pertanyaan FROM pertanyaan_materi WHERE submateri_id = '$materi[sub_materi_id]' ");
                                    while ($gtsoal = mysqli_fetch_assoc($getSoal)) {
                                    ?>
                                    <h5><?=$nomorsoal;?>. <?=$gtsoal['pertanyaan'];?></h5>
                                    <div class="test">
                                      <?php
                                        $getjawab = $con->query("SELECT *FROM pilihan_ganda WHERE pertanyaan_id = '$gtsoal[id]' ");
                                        while ($gtjawab = mysqli_fetch_assoc($getjawab)) {
                                      ?>
                                      <div class="radio">
                                        <label><input name="<?=$gtsoal['id'];?>" type="radio" value="<?=$gtjawab['pilihan_ganda']?>" required/> <?=$gtjawab['pilihan_ganda']?></label>
                                      </div>
                                      <?php }?>
                                    </div>
                                <?php $nomorsoal++;} mysqli_free_result($getjawab);
                                if(mysqli_num_rows($getSoal)>0){
                                ?>
                                    <button type="submit" class="btn btn-default">Kirim</button>
                                <?php } ?>
                                    </form>
                                  </div>
                                <?php
                                } else {
                                    $ghasil = mysqli_fetch_assoc($cekTest);
                                    echo "<span>Soal yang telah anda jawab benar = " . $ghasil['benar'] . "</span>";
                                }
                                ?>
                            </div><!-- /.box-footer -->
                        </div><!-- /.box -->        
                    <?php } mysqli_free_result($mainMateri); ?>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    
                </div><!-- /.box-footer -->
            </div><!-- /.box -->        
            

        </section><!-- /.Left col -->
        
    </div><!-- /.row (main row) -->

</section><!-- /.content -->
<?php
$_SESSION['tes']=TRUE;
}
?>
<section class="content">
    <a href="post_test.php"><button class="btn btn-info btn-block btn-flat">Mulai Tes</button></a>
</section>