<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Post test
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">post_test</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable"> 
            <!-- Box (with bar chart) -->
            <div class="box box-danger" id="loading-example">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-danger btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-danger btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /. tools -->
                    <i class="fa fa-list"></i>

                    <h3 class="box-title">Hasil Post test</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    $postTest = $con->query("SELECT nilai, kkm, nama_pengunjung FROM posttest INNER JOIN biodata_pengunjung ON biodata_pengunjung.nim = posttest.nim WHERE posttest.nim = '$data_diri' ORDER BY posttest.updated_at DESC");
                    $getposttest = mysqli_fetch_assoc($postTest);
                    $status = 'tidak lulus';
                    $tes_ulang = '<a href="post_test.php"><button class="btn btn-info">Tes Ulang</button></a>';
                    if ($getposttest['nilai'] >= $getposttest['kkm']) {
                        $status = 'lulus';
                        $tes_ulang = '';
                    }
                    ?>
                    <table>
                        <tr>
                            <td><label>NIM</label></td>
                            <td><label>:</label></td>
                            <td><label><?= $data_diri ?></label></td>
                        </tr>
                        <tr>
                            <td><label>Nama</label></td>
                            <td><label>:</label></td>
                            <td><label><?= $getposttest['nama_pengunjung'] ?></label></td>
                        </tr>
                        <tr>
                            <td><label>Nilai Kuis</label></td>
                            <td><label>:</label></td>
                            <td><label><?= $getposttest['nilai'] ?></label></td>
                        </tr>
                        <tr>
                            <td><label>KKM</label></td>
                            <td><label>:</label></td>
                            <td><label><?= $getposttest['kkm'] ?></label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>:</label></td>
                            <td><label><?= $status ?></label></td>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <?php
                    $max_test = $con->query("SELECT value FROM settings WHERE kunci = 'max_test'");
                    $get_max_test = mysqli_fetch_assoc($max_test);
                    if ($_SESSION['count_posttest'] <= ($get_max_test['value']-1)) {
                        echo $tes_ulang;   
                    }
                    ?>
                </div><!-- /.box-footer -->
            </div><!-- /.box -->        
            

        </section><!-- /.Left col -->
        
    </div><!-- /.row (main row) -->

</section><!-- /.content -->