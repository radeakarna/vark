<?php
$max_test = $con->query("SELECT value FROM settings WHERE kunci = 'max_test'");
$get_max_test = mysqli_fetch_assoc($max_test);
$_SESSION['count_posttest'] = 0;
if ($_SESSION['count_posttest'] > ($get_max_test['value']-1)) {
    echo "<script>alert('Kesenpatan anda telah habis');window.location='post_test.php?modal=hasil';</script>";   
}
?>


</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Post test
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">post_test</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content" >
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable"> 
            <!-- Box (with bar chart) -->
            <div class="box box-danger" id="loading-example">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right">
                        <!-- <button class="btn btn-danger btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-danger btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                        <span id="jam" style="margin-right:10px; padding:0"></span>
                        <!-- <p id="menit"></p>
                        <p id="detik"></p> -->
                    </div><!-- /. tools -->
                    <i class="fa fa-list"></i>

                    <h3 class="box-title">Post test</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="script/postTest.php" id="myForm">
                    <?php
                    $soal = $con->query("SELECT value FROM settings WHERE kunci = 'jumlah_soal'");
                    $jumlah_soal = mysqli_fetch_assoc($soal);
                    $nomorsoal = 1;
                    $getKuis   = $con->query("SELECT id, pilihan_ganda, pertanyaan FROM kuis ORDER BY RAND() LIMIT $jumlah_soal[value] ");
                    while ($item = mysqli_fetch_assoc($getKuis)) {
                        $pil = json_decode($item['pilihan_ganda']);
                    ?>
                    <h5><?=$nomorsoal;?>. <?=$item['pertanyaan'];?></h5>
                    <div class="test">
                      <div class="radio">
                        <label><input name="<?=$item['id'];?>" type="radio" value="<?=$pil->a?>" required/> <?=$pil->a?></label>
                      </div>

                      <div class="radio">
                        <label><input name="<?=$item['id'];?>" type="radio" value="<?=$pil->b?>" required/> <?=$pil->b?></label>
                      </div>

                      <div class="radio">
                        <label><input name="<?=$item['id'];?>" type="radio" value="<?=$pil->c?>" required/> <?=$pil->c?></label>
                      </div>

                      <div class="radio">
                        <label><input name="<?=$item['id'];?>" type="radio" value="<?=$pil->d?>" required/> <?=$pil->d?></label>
                      </div>
                    </div>

                    <?php $nomorsoal++;} mysqli_free_result($getKuis);?>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">Kirim jawaban</button>
                    </form>
                </div><!-- /.box-footer -->
            </div><!-- /.box -->        
            

        </section><!-- /.Left col -->
        
    </div><!-- /.row (main row) -->

</section><!-- /.content -->
<?php
$time_test = $con->query("SELECT value FROM settings WHERE kunci = 'time_test'");
$get_time_test = mysqli_fetch_assoc($time_test);
?>
<script>
// function startTimer(duration, display) {
//     var start = Date.now(),
//         diff,
//         minutes,
//         seconds;
//     function timer() {
//         // get the number of seconds that have elapsed since 
//         // startTimer() was called
//         diff = duration - (((Date.now() - start) / 1000) | 0);

//         // does the same job as parseInt truncates the float
//         minutes = (diff / 60) | 0;
//         seconds = (diff % 60) | 0;

//         minutes = minutes < 10 ? "0" + minutes : minutes;
//         seconds = seconds < 10 ? "0" + seconds : seconds;

//         display.textContent = minutes + ":" + seconds; 

//         if (diff <= 0) {
//             // add one second so that the count down starts at the full duration
//             // example 05:00 not 04:59
//             start = Date.now() + 1000;
//         }
//     };
//     // we don't want to wait a full second before the timer starts
//     timer();
//     setInterval(timer, 1000);
// }

// window.onload = function () {
//     var fiveMinutes = <?= $get_time_test['value']*60 ?>,
//         display = document.querySelector('#jam');
//     startTimer(fiveMinutes, display);
// };
</script>

<script type="text/javascript">
window.onload=function(){    
    var auto = setTimeout(function(){ autoRefresh(); }, 100);

    function submitform(){
      alert('Waktu telah habis');
      document.forms["myForm"].submit();
    }

    function autoRefresh(){
       clearTimeout(auto);
       auto = setTimeout(function(){ submitform(); autoRefresh(); }, <?=$get_time_test['value']*60000 ?>);
    }
}
</script>
