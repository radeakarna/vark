<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
if (!isset($_SESSION['count_posttest'])) {
    $_SESSION['count_posttest'] = 0;
}
if (!isset($_SESSION['nim'])) {
    echo "<script>alert('Silahkan mendaftar terlebih dahulu');window.location='index.php';</script>";   
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kelas | VARK</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- Icheck -->
        <link href="assets/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .soal {
              margin: 10px 10px 20px 20px;
            }

            .test {
              margin: 10px 10px 20px 20px;
            }/*
            html {
              scroll-behavior: smooth;
            }*/
        </style>
    </head>
    <body class="skin-black fixed" data-spy="scroll" data-target="#scrollspy">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Kelas
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="post_test.php">
                                <i class="fa fa-dashboard"></i> <span>Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <?php
                include "../script/koneksi.php";
                $data_diri = $_SESSION['nim'];
                if (isset($_GET['modal'])) {
                    $page = $_GET['modal'];
                    switch ($page) {
                        case 'hasil':
                            include "halaman/finish.php";
                            break;
                        default:
                            echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
                            break;
                    }
                } else {
                    include "halaman/home_test.php";
                }
                ?>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- Icheck -->
        <script src="assets/js/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- Slim scrool -->
        <script src="assets/js/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="js/countdown.jquery.js" type="text/javascript"></script>
        <script type="text/javascript">
        let today = new Date()
        console.log(today.toLocaleString('en-US', {timeZone: "Asia/Jakarta"}));
          $(function(){
              $('#jam').countdown({
                year: <?=date('Y')?>,// YYYY Format
                month: <?=date('n')?>,// 1-12
                day: <?=date('j')?>,// 1-31
                hour: today.getHours(),// 24 hour format 0-23
                minute: <?=date('i')+10?>,// 0-59
                second: 0,// 0-59
              });
            });
        </script>
    </body>
</html>