<?php
session_start();
include "../script/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kelas - Hypermedia</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/resume.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none">hypermedia</span>
        <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/log.png" alt="">
        </span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#video">Video</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#text">Text</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#gambar">Audio</a>
          </li>

        </ul>
      </div>
    </nav>

    <div class="container-fluid p-0">
    <section class="resume-section p-3 p-lg-5 d-flex d-column">
        <div class="my-auto">
          <h1 class="mb-0">HYPERMEDIA
            <span class="text-primary">VARK</span>
          </h1>
        </div>
      </section>
<?php
//menampilkan video
$getmateri = $_GET['materi'];
$getvideo  = mysqli_query($con, "SELECT main_materi.materi, main_materi.isi_materi FROM main_materi INNER JOIN sub_materi ON main_materi.sub_materi_id = sub_materi.sub_materi_id INNER JOIN materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = 1 AND materi.materi_id = ".$getmateri." ORDER BY no_urut");
while($getv      = mysqli_fetch_assoc($getvideo)){
?>
      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="video">
        <div class="my-auto">
          <h3 class="mb-0"><?=$getv['materi']?>
            <span class="text-primary"></span>
          </h3><br/>
          <p class="lead mb-5">
          <?=$getv['isi_materi']?>
          </p>

        </div>
      </section>
<?php } ?>
      <hr class="m-0">
<?php
//menampilkan video
$gettext = mysqli_query($con, "SELECT main_materi.materi, main_materi.isi_materi FROM main_materi INNER JOIN sub_materi ON main_materi.sub_materi_id = sub_materi.sub_materi_id INNER JOIN materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = 4 AND materi.materi_id = ".$getmateri." ORDER BY no_urut");
while ($gett = mysqli_fetch_assoc($gettext)) {
?>
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="text">
        <div class="my-auto">
          
            <div class="resume-content mr-auto">
              <h3 class="mb-0"><?=$gett['materi']?></h3>
              <div class="subheading mb-3">Intelitec Solutions</div>
              <p>
                <iframe src="pdf/<?=$gett['isi_materi']?>" style="width: 100%;height: 600px;border: none;"></iframe>
              </p>
            </div>
          

        </div>

      </section>
<?php } ?>

      <hr class="m-0">
<?php
//menampilkan video
$getaudio = mysqli_query($con, "SELECT main_materi.materi, main_materi.isi_materi FROM main_materi INNER JOIN sub_materi ON main_materi.sub_materi_id = sub_materi.sub_materi_id INNER JOIN materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = 3 AND materi.materi_id = ".$getmateri." ORDER BY no_urut");
while ($geta     = mysqli_fetch_assoc($getaudio)) {
?>
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="gambar">
        <div class="my-auto">
          <h2 class="mb-5"><?=$geta['materi']?></h2>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
            <audio controls>
              <source src="audio/<?=$geta['isi_materi'];?>" type="audio/mpeg" controls>
            </audio>
            </div>
          </div>
        </div>
      </section>
<?php } ?>

      <hr class="m-0">
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/resume.min.js"></script>

  </body>

</html>
