<?php
session_start();
if (!isset($_SESSION['username']) and !isset($_SESSION['status'])) {
    header("location: ../pages/login.php"); // Redirecting To Other Page
}
require_once 'config/+koneksi.php';
require_once 'models/database.php';
$connection = new Database($host, $user, $pass, $database);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/assets/css/style.css" rel="stylesheet">
    <link href="assets/assets/css/style-responsive.css" rel="stylesheet">
    <!-- Data Table -->
    <link href="../media/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="background: rgb(242, 242, 242);">

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg" style="background-color:#000000">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" style="color:#ffc100" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo" style="color:#ffc100"><b>VARK ADMIN</b></a>
            <!--logo end-->

            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="../script/logout.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse " style="background-color:#474747">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a href="profile.html"><img src="assets/assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Hello <?php echo $_SESSION['username'] . "!"; ?></h5>

                  <li class="mt">
                      <a href="?page=dashboard">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                    <li class="sub-menu">
                      <a href="?page=view_pertanyaan" >
                          <i class="fa fa-cogs"></i>
                          <span>Data Pertanyaan</span>
                      </a>

                  </li>


                  <li class="sub-menu">
                      <a href="?page=view_modalitas" >
                          <i class="fa fa-desktop"></i>
                          <span>Data Modalitas</span>
                      </a>

                  </li>


                  <li class="sub-menu">
                      <a href="?page=view_solusi" >
                          <i class="fa fa-book"></i>
                          <span>Data Solusi</span>
                      </a>

                  </li>

                  <li class="sub-menu">
                      <a href="?page=view_biodata_pengunjung" >
                          <i class="fa fa-user"></i>
                          <span>Biodata Pengunjung</span>
                      </a>

                  </li>


                  <li class="sub-menu">
                      <a href="?page=view_hasil" >
                          <i class="fa fa-tasks"></i>
                          <span>Hasil Uji</span>
                      </a>

                  </li>

                  <li class="sub-menu">
                      <a href="?page=chart" >
                          <i class="fa fa-cogs"></i>
                          <span>Diagram Sortir</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;">
                        
                        <i class="fa fa-pencil"></i>
                          <span>Kuis</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="?page=mkuis">Data Kuis</a></li>
                          <li><a  href="?page=rsult_tes">Hasil kuis (post test)</a></li>
                      </ul>

                  </li>
                  <li class="sub-menu">
                      <a href="?page=materi" >
                          <i class="fa fa-book"></i>
                          <span>Data Materi</span>
                      </a>

                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Materi</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="index.php?page=visual">Visual</a></li>
                          <li><a  href="index.php?page=auditorial">Auditorial</a></li>
                          <li><a  href="index.php?page=rw">Read/Write</a></li>
                          <li><a  href="index.php?page=kinestetik">Kinestetik</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="?page=settings" >
                          <i class="fa fa-gear"></i>
                          <span>Settings</span>
                      </a>

                  </li>

                 <!-- <li class="sub-menu">
                      <a href="?page=view_profil" >
                          <i class="fa fa-tasks"></i>
                          <span>Data Profil</span>
                      </a>

                  </li>

                  <li class="sub-menu">
                      <a href="?page=view_admin" >
                          <i class="fa fa-tasks"></i>
                          <span>Data Admin</span>
                      </a>

                  </li>-->


              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height" style="background-color:white;">

           <?php
if (@$_GET['page'] == 'dashboard' || @$_GET['page'] == '') {
    include "views/dashboard.php";
} else if (@$_GET['page'] == 'view_modalitas') {
    include "views/view_modalitas.php";
} else if (@$_GET['page'] == 'view_pertanyaan') {
    include "views/view_pertanyaan.php";
} else if (@$_GET['page'] == 'view_solusi') {
    include "views/view_solusi.php";
} else if (@$_GET['page'] == 'view_biodata_pengunjung') {
    include "views/view_biodata_pengunjung.php";
} else if (@$_GET['page'] == 'view_hasil') {
    include "views/view_hasil.php";
} else if (@$_GET['page'] == 'chart') {
    include "views/chart.php";
    //} else if (@$_GET['page'] == 'view_profil') {
    //include "views/view_profil.php";
    //} else if (@$_GET['page'] == 'view_admin') {
    // include "views/view_admin.php";
} else if (@$_GET['page'] == 'materi') {
    include "views/view_materi.php";
} else if (@$_GET['page'] == 'isi_materi') {
    include "views/isi_materi.php";
} else if (@$_GET['page'] == 'sub_materi') {
    include "views/view_sub_materi.php";
} else if (@$_GET['page'] == 'main_materi') {
    include "views/main_materi.php";
} else if (@$_GET['page'] == 'visual') {
    include "views/visual/index.php";
} else if (@$_GET['page'] == 'tambahvisual') {
    include "views/visual/tmbah.php";
} else if (@$_GET['page'] == 'editvisual') {
    include "views/visual/edit.php";
} else if (@$_GET['page'] == 'auditorial') {
    include "views/auditorial/index.php";
} else if (@$_GET['page'] == 'tambahauditorial') {
    include "views/auditorial/tmbah.php";
} else if (@$_GET['page'] == 'editauditorial') {
    include "views/auditorial/edit.php";
} else if (@$_GET['page'] == 'rw') {
    include "views/rw/index.php";
} else if (@$_GET['page'] == 'tambahrw') {
    include "views/rw/tmbah.php";
} else if (@$_GET['page'] == 'editrw') {
    include "views/rw/edit.php";
} else if (@$_GET['page'] == 'kinestetik') {
    include "views/kinestetik/index.php";
} else if (@$_GET['page'] == 'tambahkinestetik') {
    include "views/kinestetik/tmbah.php";
} else if (@$_GET['page'] == 'editkinestetik') {
    include "views/kinestetik/edit.php";
} else if (@$_GET['page'] == 'mkinestetik') {
    include "views/mkinestetik/index.php";
} else if (@$_GET['page'] == 'mtambahkinestetik') {
    include "views/mkinestetik/tmbah.php";
} else if (@$_GET['page'] == 'meditkinestetik') {
    include "views/mkinestetik/edit.php";
} else if (@$_GET['page'] == 'mauditorial') {
    include "views/mauditorial/index.php";
} else if (@$_GET['page'] == 'mtambahauditorial') {
    include "views/mauditorial/tmbah.php";
} else if (@$_GET['page'] == 'meditauditorial') {
    include "views/mauditorial/edit.php";
} else if (@$_GET['page'] == 'mrw') {
    include "views/mrw/index.php";
} else if (@$_GET['page'] == 'mtambahrw') {
    include "views/mrw/tmbah.php";
} else if (@$_GET['page'] == 'meditrw') {
    include "views/mrw/edit.php";
} else if (@$_GET['page'] == 'mpertanyaan') {
    include "views/mpertanyaan/index.php";
} else if (@$_GET['page'] == 'mtambahpertanyaan') {
    include "views/mpertanyaan/tmbah.php";
} else if (@$_GET['page'] == 'meditpertanyaan') {
    include "views/mpertanyaan/edit.php";
}else if (@$_GET['page'] == 'mvisual') {
    include "views/mvisual/index.php";
} else if (@$_GET['page'] == 'mtambahvisual') {
    include "views/mvisual/tmbah.php";
} else if (@$_GET['page'] == 'meditvisual') {
    include "views/mvisual/edit.php";
}else if (@$_GET['page'] == 'mkuis') {
    include "views/mkuis/index.php";
}else if (@$_GET['page'] == 'mtambahkuis') {
    include "views/mkuis/tmbah.php";
}else if (@$_GET['page'] == 'meditkuis') {
    include "views/mkuis/edit.php";
}else if (@$_GET['page'] == 'settings') {
    include "views/settings/settings.php";
}else if(@$_GET['page'] == 'rsult_tes'){
    include "views/mkuis/result_tes.php";
}
?>


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2017 Aplikasi Penentu Modalitas Belajar
              <!--<a href="blank.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>-->
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="jquery-3.1.1.js"></script>
    <script src="assets/assets/js/bootstrap.min.js"></script>
    <script src="assets/assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="assets/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script class="include" type="text/javascript" src="assets/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/assets/js/common-scripts.js"></script>
    <!--Jquery Data Tablw-->
    <script src="../media/js/jquery.dataTables.js"></script>
    <script src="../media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#data_table').DataTable();
      });
    </script>

    <!--script for this page-->
    <script src="assets/assets/js/chart-master/Chart.js"></script>
    <script>
      var Script = function () {
      var doughnutData = [
          {
              value: <?php echo $v; ?>,
              color:"#e1da44"
          },
          {
              value : <?php echo $a; ?>,
              color : "#aecc5a"
          },
          {
              value : <?php echo $r; ?>,
              color : "#fda02e"
          },
          {
              value : <?php echo $k; ?>,
              color : "#ab1010"
          },
          {
              value : <?php echo $va; ?>,
              color : "#7a2762"
          },
          {
              value: <?php echo $vr; ?>,
              color:"#db5461"
          },
          {
              value : <?php echo $vk; ?>,
              color : "#698f3f"
          },
          {
              value : <?php echo $ak; ?>,
              color : "#000000"
          },
          {
              value : <?php echo $ar; ?>,
              color : "#e7decd"
          },
          {
              value : <?php echo $rk; ?>,
              color : "#804e49"
          },
          {
              value : <?php echo $ark; ?>,
              color : "#80d8df"
          },
          {
              value: <?php echo $vrk; ?>,
              color: "#b5838d"
          },
          {
              value : <?php echo $vak; ?>,
              color : "#36006f"
          },
          {
              value : <?php echo $var; ?>,
              color : "#98b287"
          },
          {
              value : <?php echo $vark; ?>,
              color : "#9a7257"
          }

      ];

      new Chart(document.getElementById("doughnut").getContext("2d")).Doughnut(doughnutData);


  }();
    </script>

  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>



  </body>
</html>
