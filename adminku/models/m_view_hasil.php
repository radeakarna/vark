<?php
class Hasil {  

	private $mysqli;

	function __construct($conn) {
		$this->mysqli = $conn;
	}

	public function tampil($nim = null) {
		$db = $this->mysqli->conn;
		$sql = "SELECT * FROM biodata_pengunjung inner join hasil ON biodata_pengunjung.nim = hasil.nim";
		if($nim != null) {
            $sql .= " WHERE nim = $nim";
		}
		$query = $db->query($sql) or die ($db->error);
        return $query;
	}


	public function edit($sql) {
		$db = $this->mysqli->conn;
		$db->query($sql) or die ($db->error);
	}

	public function hapus($nim) {
		$db = $this->mysqli->conn;
		$db->query("DELETE FROM hasil WHERE nim='$nim' ") or die ($db->error);
		$db->query("DELETE FROM biodata_pengunjung WHERE nim='$nim' ") or die ($db->error);
	}

	function __destruct() {
		$db = $this->mysqli->conn;
		$db->close();
 	}
}
?>