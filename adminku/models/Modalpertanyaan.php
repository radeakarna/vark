<?php
class Modalpertanyaan
{
    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil($as)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM pertanyaan_materi where submateri_id = '$as'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function pg($pertanyaan_id){
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM pilihan_ganda where pertanyaan_id = '$pertanyaan_id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function tambah($sub_materi_id, $pertanyaan, $kunci)
    {
        $db    = $this->mysqli->conn;
        $query = $db->query("INSERT INTO pertanyaan_materi (submateri_id, pertanyaan, kunci) VALUES ('$sub_materi_id', '$pertanyaan', '$kunci')") or die($db->error);
        return $query;
    }
    public function getLastId(){
        $db    = $this->mysqli->conn;
        return $db->insert_id;
    }
    public function tambahPg($pertanyaan_id,$pg){
        $db    = $this->mysqli->conn;
        $query = $db->query("INSERT INTO pilihan_ganda (pertanyaan_id, pilihan_ganda) VALUES ('$pertanyaan_id', '$pg')") or die($db->error);
    }
    public function hapus($id,$sub_materi_id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM pertanyaan_materi WHERE id='$id' and submateri_id='$sub_materi_id'") or die($db->error);
        $db->query("DELETE FROM pilihan_ganda WHERE pertanyaan_id='$id'") or die($db->error);
    }
    public function getPertanyaan($id,$sub_materi_id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM pertanyaan_materi where id='$id' and submateri_id = '$sub_materi_id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getPilgan($pertanyaan_id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM pilihan_ganda where pertanyaan_id='$pertanyaan_id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }

    public function getMakul($table)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getSmateri($table, $id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table WHERE main_materi_id ='$id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function edit($id,$pertanyaan,$kunci)
    {
        $db = $this->mysqli->conn;
        $sql = "UPDATE pertanyaan_materi SET pertanyaan = '$pertanyaan', kunci = '$kunci' WHERE id = '$id'";
        $db->query($sql) or die($db->error);
    }
    public function editPg($id,$pilihan_ganda)
    {
        $db = $this->mysqli->conn;
        $sql = "UPDATE pilihan_ganda SET pilihan_ganda = '$pilihan_ganda' WHERE id = '$id'";
        $db->query($sql) or die($db->error);
    }
    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
