<?php
class Mainmateri
{

    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil($id = null)
    {
        $db  = $this->mysqli->conn;
        $sql = "SELECT * FROM main_materi";
        if ($id != null) {
            $sql .= " WHERE sub_materi_id = $id";
        }
        $query = $db->query($sql) or die($db->error);
        return $query;
    }

    public function tambah($materi, $isi_materi, $sub_materi_id)
    {
        $db = $this->mysqli->conn;
        $db->query("INSERT INTO main_materi(sub_materi_id, materi, isi_materi) VALUES ('$sub_materi_id','$materi','$isi_materi')") or die($db->error);
        return $query;
    }

    public function edit($sql)
    {
        $db = $this->mysqli->conn;
        $db->query($sql) or die($db->error);
    }

    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM modalitas WHERE id_modalitas='$id' ") or die($db->error);
    }

    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
