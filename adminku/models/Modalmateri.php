<?php
class Modalmateri
{
    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil()
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM materi INNER JOIN sub_materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = '1' ORDER BY materi ASC";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function Ttampil($modalitas, $as)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM main_materi INNER JOIN sub_materi ON main_materi.sub_materi_id = sub_materi.sub_materi_id WHERE id_solusi = '$modalitas' AND main_materi.sub_materi_id = '$as' ORDER BY no_urut ASC";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getMakul($table)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getSmateri($table, $id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table WHERE main_materi_id ='$id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function tambah($sub_materi_id, $materi, $isi_materi, $urutan)
    {
        $db    = $this->mysqli->conn;
        $query = $db->query("INSERT INTO main_materi (sub_materi_id, materi, isi_materi, no_urut) VALUES ('$sub_materi_id', '$materi', '$isi_materi', '$urutan')") or die($db->error);
        return $query;
    }
    public function edit($sql)
    {
        $db = $this->mysqli->conn;
        $db->query($sql) or die($db->error);
    }
    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM main_materi WHERE main_materi_id='$id' ") or die($db->error);
    }
    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
