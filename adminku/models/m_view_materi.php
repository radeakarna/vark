<?php
class Materi
{

    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil($id = null)
    {
        $db  = $this->mysqli->conn;
        $sql = "SELECT * FROM materi";
        if ($id != null) {
            $sql .= " WHERE id_modalitas = $id";
        }
        $query = $db->query($sql) or die($db->error);
        return $query;
    }

    public function tambah($materi)
    {
        $db = $this->mysqli->conn;
        $db->query("INSERT INTO materi VALUES (NULL, '$materi')") or die($db->error);
        return $query;
    }

    public function edit($sql)
    {
        $db = $this->mysqli->conn;
        $db->query($sql) or die($db->error);
    }

    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM materi WHERE materi_id='$id' ") or die($db->error);
    }

    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
