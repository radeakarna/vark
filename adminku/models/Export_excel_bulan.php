<?php
include"../../script/koneksi.php";
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../plugins/phpexcel/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A5', 'No')
            ->setCellValue('B5', 'Nim')
            ->setCellValue('C5', 'Nama')
            ->setCellValue('D5', 'Nilai')
            ->setCellValue('E5', 'Keterangan');

// Miscellaneous glyphs, UTF-8
$hasil = $objPHPExcel->setActiveSheetIndex(0);
	$no = 1;
	$counter = 6;
	$bulan_begin = $_POST['bulan_begin'];
	$tahun_begin = $_POST['tahun_begin'];
	$bulan_finish = $_POST['bulan_finish'];
	$tahun_finish = $_POST['tahun_finish'];
	$query = mysqli_query($con,"SELECT posttest.kkm, posttest.nim, posttest.nilai, biodata_pengunjung.nama_pengunjung FROM posttest INNER JOIN results_posttest ON posttest.id = results_posttest.id_posttest INNER JOIN biodata_pengunjung ON results_posttest.nim = biodata_pengunjung.nim WHERE month(update_at) between '$bulan_begin' and '$bulan_finish' and year(update_at) between '$tahun_begin' and '$tahun_finish'");
	while ($r = mysqli_fetch_array($query)) {
		$hasilTes = 'Tidak lulus';
		if ($r['nilai'] >= $r['kkm']) {
			$hasilTes = 'Lulus';
		}
		$hasil->setCellValue('A'.$counter,$no);
		$hasil->setCellValue('B'.$counter,$r['nim']);
		$hasil->setCellValue('C'.$counter,$r['nama_pengunjung']);
		$hasil->setCellValue('D'.$counter,$r['nilai']);
		$hasil->setCellValue('E'.$counter,$hasilTes);
		$counter++;
		$no++;
	}
	
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Data formatur');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
