<?php
class Profil { 

	private $mysqli;

	function __construct($conn) {
		$this->mysqli = $conn;
	}

	public function tampil($id = null) {
		$db = $this->mysqli->conn;
		$sql = "SELECT * FROM profil";
		if($id != null) {
            $sql .= " WHERE id_profil = $id";
		}
		$query = $db->query($sql) or die ($db->error);
        return $query;
	}

	//public function tambah($id_solusi, $jns_modalitas, $intake, $output, $swot) {
	//	$db = $this->mysqli->conn;
	//	$db->query("INSERT INTO solusi VALUES ('$id_solusi', '$jns_modalitas', '$intake', '$output', $swot)") or die ($db->error);
    //    return $query;
	//}

	public function edit($sql) {
		$db = $this->mysqli->conn;
		$db->query($sql) or die ($db->error);
	}

	//public function hapus($id) {
	//	$db = $this->mysqli->conn;
	//	$db->query("DELETE FROM solusi WHERE id_solusi='$id' ") or die ($db->error);
	//}

	function __destruct() {
		$db = $this->mysqli->conn;
		$db->close();
 	}
}
?>