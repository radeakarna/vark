<?php
class Visual
{

    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil()
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM materi INNER JOIN sub_materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = '1' ORDER BY materi.materi_id ASC";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function Ttampil($modalitas)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM materi INNER JOIN sub_materi ON materi.materi_id = sub_materi.materi_id WHERE id_solusi = '$modalitas' ORDER BY materi.materi_id ASC";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getMakul($table)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getSmateri($table, $id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table WHERE sub_materi_id ='$id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function tambah($id_modalitas, $id_materi, $jns_modalitas, $keterangan)
    {
        $db    = $this->mysqli->conn;
        $query = $db->query("INSERT INTO sub_materi (materi_id, id_solusi, nama_materi, keterangan) VALUES ('$id_materi','$id_modalitas','$jns_modalitas','$keterangan')") or die($db->error);
        return $query;
    }
    public function edit($sql)
    {
        $db = $this->mysqli->conn;
        $db->query($sql) or die($db->error);
    }
    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM sub_materi WHERE sub_materi_id='$id' ") or die($db->error);
        $db->query("DELETE FROM main_materi WHERE sub_materi_id='$id' ") or die($db->error);
    }
    public function getMmateri($modalitas, $as)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM main_materi INNER JOIN sub_materi ON main_materi.sub_materi_id = sub_materi.sub_materi_id WHERE id_solusi = '$modalitas' AND main_materi.sub_materi_id = '$as' ORDER BY no_urut ASC";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
