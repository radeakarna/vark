<?php
class Submateri
{

    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil($id = null)
    {
        $db  = $this->mysqli->conn;
        $sql = "SELECT * FROM sub_materi";
        if ($id != null) {
            $sql .= " WHERE sub_materi_id = $id";
        }
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function gaetwhere($materi_id = null, $id_solusi)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM sub_materi INNER JOIN materi ON sub_materi.materi_id = materi.materi_id INNER JOIN solusi ON sub_materi.id_solusi = solusi.id_solusi WHERE sub_materi.materi_id = '$materi_id' AND sub_materi.id_solusi = '$id_solusi'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }

    public function tambah($id_modalitas, $id_materi, $isi_materi)
    {
        $db = $this->mysqli->conn;
        $db->query("INSERT INTO sub_materi(materi_id, id_solusi, nama_materi) VALUES ('$id_modalitas', '$id_materi', '$isi_materi')") or die($db->error);
        return $query;
    }

    public function edit($sql)
    {
        $db = $this->mysqli->conn;
        $db->query($sql) or die($db->error);
    }

    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM sub_materi WHERE sub_materi_id='$id' ") or die($db->error);
    }

    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
