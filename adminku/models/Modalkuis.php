<?php
class Modalkuis
{
    private $mysqli;

    public function __construct($conn)
    {
        $this->mysqli = $conn;
    }

    public function tampil()
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM kuis";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function tambah($pertanyaan, $kunci, $pilihan_ganda)
    {
        $db    = $this->mysqli->conn;
        $query = $db->query("INSERT INTO kuis (pertanyaan, kunci, pilihan_ganda) VALUES ('$pertanyaan', '$kunci', '$pilihan_ganda')") or die($db->error);
        return $query;
    }
    public function getLastId(){
        $db    = $this->mysqli->conn;
        return $db->insert_id;
    }
    public function hapus($id)
    {
        $db = $this->mysqli->conn;
        $db->query("DELETE FROM kuis WHERE id='$id'") or die($db->error);
    }
    public function getPertanyaan($id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM kuis where id='$id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getPilgan($pertanyaan_id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM pilihan_ganda where pertanyaan_id='$pertanyaan_id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }

    public function getMakul($table)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function getSmateri($table, $id)
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT * FROM $table WHERE main_materi_id ='$id'";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function edit($id,$pertanyaan,$kunci,$pilihan_ganda)
    {
        $db = $this->mysqli->conn;
        $sql = "UPDATE kuis SET pertanyaan = '$pertanyaan', kunci = '$kunci', pilihan_ganda = '$pilihan_ganda' WHERE id = '$id'";
        $db->query($sql) or die($db->error);
    }
    public function editPg($id,$pilihan_ganda)
    {
        $db = $this->mysqli->conn;
        $sql = "UPDATE pilihan_ganda SET pilihan_ganda = '$pilihan_ganda' WHERE id = '$id'";
        $db->query($sql) or die($db->error);
    }
    public function getResultKuis()
    {
        $db    = $this->mysqli->conn;
        $sql   = "SELECT posttest.kkm, posttest.nim, posttest.nilai, biodata_pengunjung.nama_pengunjung FROM posttest INNER JOIN results_posttest ON posttest.id = results_posttest.id_posttest INNER JOIN biodata_pengunjung ON results_posttest.nim = biodata_pengunjung.nim";
        $query = $db->query($sql) or die($db->error);
        return $query;
    }
    public function __destruct()
    {
        $db = $this->mysqli->conn;
        $db->close();
    }
}
