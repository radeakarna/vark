<?php
error_reporting(0);
include "models/Modalkuis.php";
$vm = new Modalkuis($connection);
$pilihan_ganda == 'false' ? $valuePG = "&pg=false" : $valuePG = null ;

?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Daftar Pertanyaan Kuis</span>
    <a href="index.php?page=mtambahkuis"><button type="button" class="btn btn-default pull-right"> <i class="fa fa-plus"></i> Tambah Pertanyaan</button></a>
    </div>
</div>

<div class="row mt">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="data_table">
            <thead>
            <tr>
                <th><center>NO.</center></th>
                <th><center>Pertanyaan</center></th>
                <th><center>Kunci</center></th>
                <?php if ($pilihan_ganda != 'false'){
                    echo '<th><center>Pilihan Ganda</center></th>';
                 } ?>
                
                <th><center>Opsi</center></th>
            </tr>
            </thead>
<?php
$no     = 1;
$tampil = $vm->tampil();
while ($data = $tampil->fetch_object()) {
    
    $pil = json_decode($data->pilihan_ganda);
    ?>
            <tr>
            <td align="center"><?php echo $no++ . "."; ?></td>
            <td><?php echo $data->pertanyaan; ?></td>
            <td><?php echo $data->kunci; ?></td>
            <?php if ($pilihan_ganda != 'false'){?>
                <td>
                <ol type="a">
                <li><?php echo $pil->a; ?></li>
                <li><?php echo $pil->b; ?></li>
                <li><?php echo $pil->c; ?></li>
                <li><?php echo $pil->d; ?></li>
                </ol>
                </td>
            <?php } ?>
            
            <td align="center">
                <a href="index.php?page=meditkuis&id=<?php echo $data->id; ?><?=$valuePG?>">
                <button class="btn btn-info btn-xs" data-toggle='tooltip' title='Edit Data'><i class="fa fa-edit"></i></button>
                </a>
                <a href="?page=mkuis&act=del&id=<?php echo $data->id; ?><?=$valuePG?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                <button class="btn btn-danger btn-xs" data-toggle='tooltip' title='Hapus Data'><i class="fa fa-trash-o"></i></button>
                </a>
            </td>
            </tr>
        <?php
}?>
            </table>
        </div>
    </div>
</div>
<?php
if (@$_GET['act'] == 'del') {
    $id = $_GET['id'];
    $vm->hapus($id);
    ?>
    <script>
        document.location='?page=mkuis<?=$valuePG?>';
    </script>
<?php }?>