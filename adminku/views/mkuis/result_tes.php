<?php
error_reporting(0);
include "models/Modalkuis.php";
$vm = new Modalkuis($connection);
?>
<div class="mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Daftar Nilai Kuis</span>
    <a href="models/Export_excel.php"><button type="button" class="btn btn-success pull-right"> <i class="fa fa-plus"></i> Export all</button></a>
</div>
<br>
<div class="mt">
    <form method="POST" action="models/Export_excel_bulan.php">
        <div class="form-group">
            <div class="col-sm-2">
                <select name="bulan_begin" class="form-control">
                <?php
                    for ($i=1; $i <= 12 ; $i++) { 
                ?>  
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="col-sm-3">
                <select name="tahun_begin" class="form-control">
                <?php
                    for ($i=2018; $i <= date('Y') ; $i++) { 
                ?>  
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="col-sm-2">
                <select name="bulan_finish" class="form-control">
                <?php
                    for ($i=1; $i <= 12 ; $i++) { 
                ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="col-sm-3">
                <select name="tahun_finish" class="form-control">
                <?php
                    for ($i=2018; $i <= date('Y') ; $i++) { 
                ?>  
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="submit" class="btn btn-success pull-right" name="export" value="export data excell">
            </div>
        </div>
    </form>
</div>
<br>
<div class="mt">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="data_table">
            <thead>
            <tr>
                <th><center>NO.</center></th>
                <th><center>NIM</center></th>
                <th><center>Nama</center></th>
                <th><center>Nilai</center></th>
                <th><center>Keterangan</center></th>
            </tr>
            </thead>
            <?php
            $no     = 1;
            $tampil = $vm->getResultKuis();
            while ($data = $tampil->fetch_object()) {
            ?>
            <tr>
                <td align="center"><?= $no++ . "."; ?></td>
                <td><?= $data->nim; ?></td>
                <td><?= $data->nama_pengunjung; ?></td>
                <td><?= $data->nilai; ?></td>
                <td>
                    <?php
                        if ($data->nilai < $data->kkm) {
                            echo "Tidak Lulus";
                        }else{
                            echo "Lulus";
                        }
                     ?>
                </td>
            </tr>
            <?php } ?>
            </table>
        </div>
    </div>
</div>
<?php
if (@$_GET['act'] == 'del') {
    $id = $_GET['id'];
    $vm->hapus($id);
    ?>
    <script>
        document.location='?page=mkuis<?=$valuePG?>';
    </script>
<?php }?>