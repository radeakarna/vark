<?php
include "models/settings/ModalSettings.php";
$settings = new ModalSettings($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Settings</span>
    </div>
</div>
<div class="row mt">
    <?php
        $kkm        = $settings->tampil("kkm");
        $get_kkm    = $kkm->fetch_object();
    ?>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><i class="fa fa-tasks"></i> Setting batas nilai (KKM)</div>
            <div class="panel-body">
                <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label">KKM</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="kkm" value="<?=$get_kkm->value?>" required/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <input type="submit"class="btn btn-success" class="form-control" name="update_kkm" value="Simpan">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
        $jumlah_soal        = $settings->tampil("jumlah_soal");
        $get_jumlah_soal    = $jumlah_soal->fetch_object();
    ?>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><i class="fa fa-tasks"></i> Setting jumlah soal post test</div>
            <div class="panel-body">
                <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label">JUMLAH SOAL</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="jumlah_soal" value="<?= $get_jumlah_soal->value ?>" required/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <input type="submit"class="btn btn-success" class="form-control" name="update_jumlah_soal" value="Simpan">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
        $max_test        = $settings->tampil("max_test");
        $get_max_test    = $max_test->fetch_object();
    ?>
    <div class="col-lg-6">
        <div class="panel panel-success">
            <!-- Default panel contents -->
            <div class="panel-heading"><i class="fa fa-tasks"></i> Setting maksimal post test</div>
            <div class="panel-body">
                <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label">MAKSIMAL TEST</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_test" value="<?= $get_max_test->value ?>" required/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <input type="submit"class="btn btn-success" class="form-control" name="update_max_test" value="Simpan">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
        $time_test        = $settings->tampil("time_test");
        $get_time_test    = $time_test->fetch_object();
    ?>
    <div class="col-lg-6">
        <div class="panel panel-success">
            <!-- Default panel contents -->
            <div class="panel-heading"><i class="fa fa-tasks"></i> Setting Waktu pengerjaan post test</div>
            <div class="panel-body">
                <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label">MAKSIMAL WAKTU PENGERJAAN</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="time_test" value="<?= $get_time_test->value ?>" required/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <input type="submit"class="btn btn-success" class="form-control" name="update_time_test" value="Simpan">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($_POST['update_kkm'])) {
    $value_kkm = $connection->conn->real_escape_string($_POST['kkm']);
    $settings->update('KKM',$value_kkm);
    ?>
    <script>
        alert('Data berhasil diubah');
        document.location='?page=settings';
    </script>
<?php }
if (isset($_POST['update_jumlah_soal'])) {
    $value = $connection->conn->real_escape_string($_POST['jumlah_soal']);
    $settings->update('jumlah_soal',$value);
    ?>
    <script>
        alert('Data berhasil diubah');
        document.location='?page=settings';
    </script>
<?php }
if (isset($_POST['update_max_test'])) {
    $value = $connection->conn->real_escape_string($_POST['max_test']);
    $settings->update('max_test',$value);
    ?>
    <script>
        alert('Data berhasil diubah');
        document.location='?page=settings';
    </script>
<?php }
if (isset($_POST['update_time_test'])) {
    $value = $connection->conn->real_escape_string($_POST['time_test']);
    $settings->update('time_test',$value);
    ?>
    <script>
        alert('Data berhasil diubah');
        document.location='?page=settings';
    </script>
<?php } ?>