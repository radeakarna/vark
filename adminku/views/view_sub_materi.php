<?php
include "models/m_view_sub_materi.php";

$vm = new Submateri($connection);

if (@$_GET['act'] == '') {
    ?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Sub Materi</h3>

              </div>
            </div>

            <div class="row mt">
              <div class="col-lg-12">

                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th><center>NO.</center></th>
                      <th><center>MODALITAS</center></th>
                      <th><center>Materi</center></th>
                      <th><center>Sub Materi</center></th>
                      <th><center>OPSI</center></th>
                    </tr>
                    </thead>
                      <?php
$no     = 1;
    $tampil = $vm->gaetwhere($_GET['mpdalitas'], $_GET['asc']);
    while ($data = $tampil->fetch_object()) {
        ?>

                    <tr>
                    <td align="center"><?php echo $no++ . "."; ?></td>
                    <td><?php echo $data->jns_modalitas; ?></td>
                    <td><?php echo $data->materi; ?></td>
                    <td><?php echo $data->nama_materi; ?></td>
                      <td align="center">
                        <a href="index.php?page=main_materi&as=<?php echo $data->sub_materi_id; ?>">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Isi Materi</button>
                        </a>
                        <a id="edit_modalitas" data-toggle="modal" data-target="#edit" data-idm="<?php echo $data->sub_materi_id; ?>" data-idp="<?php echo $data->materi_id; ?>" data-jnsm="<?php echo $data->id_solusi; ?>" data-desm="<?php echo $data->nama_materi; ?>">
                        <button class="btn btn-info btn-xs" onclick="dttampildua('<?php echo $data->sub_materi_id; ?>')"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=sub_materi&act=del&id=<?php echo $data->sub_materi_id; ?>&mpdalitas=<?php echo $_GET['mpdalitas']; ?>&asc=<?php echo $_GET['asc']; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button>
                        </a>
                      </td>
                    </tr>
                      <?php
}?>
                  </table>
                </div>


                  <botton type="button" class="btn btn-success" data-toggle="modal" onclick="dttampil()" data-target="#tambah"> Tambah Data</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data sub materi</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                              <div class="form-group">
                                 <input type="hidden" name="id_solusi" class="form-control" value="<?php echo $_GET['mpdalitas']; ?>" id="id_modalitas" required>
                              </div>
                               <div class="form-group">
                                 <input type="hidden" name="id_materi" class="form-control" value="<?php echo $_GET['asc']; ?>" id="id_pertanyaan" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">Materi</label>
                                 <input type="text" name="materi" class="form-control" id="materi" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php

    if (isset($_POST['tambah'])) {
        $id_modalitas  = $_GET['mpdalitas'];
        $id_materi     = $_GET['asc'];
        $jns_modalitas = $connection->conn->real_escape_string($_POST['materi']);

        $vm->tambah($id_modalitas, $id_materi, $jns_modalitas);
        //header("location:?page=view_modalitas");
        ?>
                                    <script>
                                        document.location='?page=sub_materi&mpdalitas=<?php echo $_GET['mpdalitas']; ?>&asc=<?php echo $_GET['asc']; ?>';
                                    </script>
                                <?php
}
    ?>

                      </div>
                    </div>
                  </div>

                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Modalitas</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                               <div class="form-group">
                                 <input type="hidden" name="id_sub_materi" class="form-control" id="id_sub_materi" required>
                                 <input type="hidden" name="id_modalitas" class="form-control" value="<?php echo $_GET['mpdalitas']; ?>" id="id_modalitas" required>
                              </div>

                              <div class="form-group">
                                 <input type="hidden" name="id_pertanyaan" class="form-control" value="<?php echo $_GET['asc']; ?>" id="id_pertanyaan" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">Deskripsi Modalitas</label>
                                 <input type="text" name="materi" class="form-control" id="materi" required="required"/>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>
                      </div>
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_modalitas", function() {
                          var id_sub_materi = $(this).data('idm');
                          var deskripsimateri = $(this).data('desm');
                          $("#modal-edit #id_sub_materi").val(id_sub_materi);
                          //$("#modal-edit #id_pertanyaan").val(idpertanyaan);
                          //$("#modal-edit #jns_modalitas").val(jenismodalitas);
                          $("#modal-edit #materi").val(deskripsimateri);
                      });

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_sub_materi.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>
                  </div>

                </div>
             </div>

<?php
} else if (@$_GET['act'] == 'del') {
    $vm->hapus($_GET['id']);
    //header("location:?page=view_modalitas");
    ?>
                                    <script>
                                        document.location='?page=sub_materi&mpdalitas=<?php echo $_GET['mpdalitas']; ?>&asc=<?php echo $_GET['asc']; ?>';
                                    </script>
                                <?php
}
