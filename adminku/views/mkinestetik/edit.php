<?php
include "models/Modalmateri.php";
$vm        = new Modalmateri($connection);
$as        = $_GET['as'];
$id        = $_GET['id'];
$getMateri = $vm->getSmateri('main_materi', $id);
$r         = $getMateri->fetch_object();
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Materi Kinestetik</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Edit data</div>
        <div class="panel-body">
        <form class="form-horizontal style-form" method="POST">
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Judul materi</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="judul" value="<?=$r->materi?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Frame video </label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="materi"><?=$r->isi_materi?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">No urut ditampilkan</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="urutan" value="<?=$r->no_urut?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label"></label>
                <div class="col-sm-10">
                    <input type="submit"class="btn btn-success" class="form-control" name="edit" value="Edit data">
                </div>
            </div>
        </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['edit'])) {
    $judul  = $connection->conn->real_escape_string($_POST['judul']);
    $materi = $connection->conn->real_escape_string($_POST['materi']);
    $urutan = $_POST['urutan'];
    $sql    = "UPDATE main_materi SET materi = '$judul', isi_materi = '$materi', no_urut = '$urutan' WHERE main_materi_id = '$id'";
    $vm->edit($sql);
    ?>
    <script>
        alert('Data berhasil di update');
        document.location='?page=mkinestetik&as=<?=$as?>';
    </script>
<?php }?>