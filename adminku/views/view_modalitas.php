<?php
include "models/m_view_modalitas.php";  

$vm = new Modalitas($connection);

if(@$_GET['act'] == '') {
?>
            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Modalitas</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th><center>NO.</center></th>
                      <th><center>ID PERTANYAAN</center></th>
                      <th><center>JENIS MODALITAS</center></th>
                      <th><center>DESKRIPSI MODALITAS</center></th>
                      <th><center>Nomor ditampilkan</center></th>
                      <th><center>OPSI</center></th>
                    </tr>
                    </thead>
                      <?php
                        $no = 1;
                        $tampil = $vm->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->id_pertanyaan; ?></td> 
                    <td><?php echo $data->jns_modalitas; ?></td>
                    <td><?php echo $data->des_modalitas; ?></td>  
                    <td><?php echo $data->nomor; ?></td>  
                      <td align="center">
                        <a id="edit_modalitas" data-toggle="modal" data-target="#edit" data-idm="<?php echo $data->id_modalitas; ?>" data-idp="<?php echo $data->id_pertanyaan; ?>" data-jnsm="<?php echo $data->jns_modalitas; ?>" data-desm="<?php echo $data->des_modalitas; ?>" data-nomor="<?php echo $data->nomor; ?>">
                        <button class="btn btn-info btn-xs" onclick="dttampildua('<?php echo $data->id_modalitas; ?>')"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_modalitas&act=del&id=<?php echo $data->id_modalitas; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  <botton type="button" class="btn btn-success" data-toggle="modal" onclick="dttampil()" data-target="#tambah"> Tambah Data</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data Modalitas</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                              <div class="form-group">
                                 <label class="control-label" for="id_pertanyaan">ID Pertanyaan</label>
                                  <select class="form-control" id="tampil_disini" name="tampil_idpertanyaan">
                                    <option ></option>
                                  </select>
                              </div>
                               <div class="form-group">
                                 <label class="control-label" for="jns_modalitas">Jenis Modalitas</label>
                                 <select name="jns_modalitas" class="form-control">
                                    <option value = "Visual">Visual</option>
                                    <option value = "Auditorial">Auditorial</option>
                                    <option value = "Read-Write">Read-Write</option>
                                    <option value = "Kinestetik">Kinestetik</option>
                                 </select>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">Deskripsi Modalitas</label>
                                 <input type="text" name="des_modalitas" class="form-control" id="des_modalitas" required>
                              </div>
                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">No urut ditampilkan</label>
                                 <input type="number" name="nomor" min="1" max="6" class="form-control" id="nomor" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php
                                
                              if(isset($_POST['tambah'])){
                                $id_modalitas  = $connection->conn->real_escape_string($_POST['id_modalitas']);
                                $id_pertanyaan = $connection->conn->real_escape_string($_POST['tampil_idpertanyaan']);
                                $jns_modalitas = $connection->conn->real_escape_string($_POST['jns_modalitas']);
                                $des_modalitas = $connection->conn->real_escape_string($_POST['des_modalitas']);
                                $nomor = $_POST['nomor'];
                                
                                $vm->tambah($id_pertanyaan, $jns_modalitas, $des_modalitas,$nomor);
                                //header("location:?page=view_modalitas");
                                  ?>
                                    <script>
                                        document.location='?page=view_modalitas';
                                    </script>
                                <?php
                              }
                          ?>
                          
                      </div>  
                    </div>
                  </div>
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Modalitas</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                               <div class="form-group">
                                 <label class="control-label" for="id_modalitas">ID Modalitas</label>
                                 <input type="text" name="id_modalitas" class="form-control" id="id_modalitas" required>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="id_pertanyaan">ID Pertanyaan</label>
                                 <input type="text" name="id_pertanyaan" class="form-control" id="id_pertanyaan" required>
                              </div>
                               <div class="form-group">
                                 <label class="control-label" for="jns_modalitas">Jenis Modalitas</label>
                                 <select name="jns_modalitas" class="form-control" >
                                    <option id="tampil_disini2"></option>
                                    <option value = "Visual">Visual</option>
                                    <option value = "Auditorial">Auditorial</option>
                                    <option value = "Read-Write">Read-Write</option>
                                    <option value = "Kinestetik">Kinestetik</option>
                                 </select>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">Deskripsi Modalitas</label>
                                 <input type="text" name="des_modalitas" class="form-control" id="des_modalitas" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">No urut ditampilkan</label>
                                 <input type="number" name="nomor" min="1" max="6" class="form-control" id="nomor" required>
                              </div>

                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_modalitas", function() {
                          var idmodalitas = $(this).data('idm');
                          var idpertanyaan = $(this).data('idp');
                          var jenismodalitas = $(this).data('jnsm');
                          var deskripsimodalitas = $(this).data('desm');
                          var nomor = $(this).data('nomor');
                          $("#modal-edit #id_modalitas").val(idmodalitas);
                          $("#modal-edit #id_pertanyaan").val(idpertanyaan);
                          $("#modal-edit #jns_modalitas").val(jenismodalitas);
                          $("#modal-edit #des_modalitas").val(deskripsimodalitas);
                          $("#modal-edit #nomor").val(nomor);
                      });
                        
                      function dttampil(){
                          $(document).ready(function() {   
                            var data_table = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select.php",             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table += data[ii].option;
                                         }

                                        $('#tampil_disini').html(data_table);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }
                      function dttampildua(x){
                          $(document).ready(function() {   
                            var data_table2 = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select_2.php",
                                    data: {id:x},             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table2 += data[ii].option;
                                         }

                                        $('#tampil_disini2').html(data_table2);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_modalitas.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $vm->hapus($_GET['id']);
    //header("location:?page=view_modalitas");
     ?>
                                    <script>
                                        document.location='?page=view_modalitas';
                                    </script>
                                <?php
}              
                  