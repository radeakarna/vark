<?php
include "models/m_view_materi.php";

$vm = new Materi($connection);

if (@$_GET['act'] == '') {
    ?>
            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Materi</h3>

              </div>
            </div>

            <div class="row mt">
              <div class="col-lg-12">

                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th><center>NO.</center></th>
                      <th><center>Materi</center></th>
                      <th><center>OPSI</center></th>
                    </tr>
                    </thead>
                      <?php
$no     = 1;
    $tampil = $vm->tampil();
    while ($data = $tampil->fetch_object()) {
        ?>

                    <tr>
                    <td align="center"><?php echo $no++ . "."; ?></td>
                    <td><?php echo $data->materi; ?></td>
                      <td align="center">
                      <!-- <a href="?page=isi_materi&asc=<?php echo $data->materi_id; ?>">
                        <button class="btn btn-success btn-xs"><i class="fa fa-search"></i> Isi Materi</button>
                        </a> -->
                        <a id="edit_modalitas" data-toggle="modal" data-target="#edit" data-idm="<?php echo $data->materi_id; ?>" data-idp="<?php echo $data->materi; ?>" ">
                        <button class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=materi&act=del&id=<?php echo $data->materi_id; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button>
                        </a>
                      </td>
                    </tr>
                      <?php
}?>
                  </table>
                </div>


                  <botton type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah"> Tambah Data</botton>

                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data Mmateri</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                              <div class="form-group">
                                 <label class="control-label" for="des_modalitas">Nama BAB</label>
                                 <input type="text" name="des_materi" class="form-control" id="des_materi" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php
if (isset($_POST['tambah'])) {
        $des_materi = $connection->conn->real_escape_string($_POST['des_materi']);

        $vm->tambah($des_materi);
        ?>
                                    <script>
                                        document.location='?page=materi';
                                    </script>
                                <?php
}
    ?>
                      </div>
                    </div>
                  </div>

                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Materi</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                               <div class="form-group">
                                 <label class="control-label" for="id_modalitas">ID Materi</label>
                                 <input type="text" name="id_materi" class="form-control" id="id_materi" required>
                                </div>
                                <div class="form-group">
                                 <label class="control-label" for="id_modalitas">BAB</label>
                                 <input type="text" name="materi" class="form-control" id="materi" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Update">
                            </div>
                          </form>
                      </div>
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_modalitas", function() {
                          var idmateri = $(this).data('idm');
                          var materi = $(this).data('idp');
                          $("#modal-edit #id_materi").val(idmateri);
                          $("#modal-edit #materi").val(materi);
                      });

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_materi.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>
                  </div>

                </div>
             </div>

<?php
} else if (@$_GET['act'] == 'del') {
    $vm->hapus($_GET['id']);
    ?>
    <script>
        document.location='?page=materi';
    </script>
<?php
}
