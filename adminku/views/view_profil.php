<?php
include "models/m_view_profil.php";  

$vp = new Profil($connection);

?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i>Data Profil</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                      <tr>
                        <th><center>NO.</center></th>
                        <th><center>JENIS MODALITAS</center></th>
                        <th><center>KARAKTER</center></th>
                        <th><center>OPSI</center></th>
                      </tr>
                    </thead>
                      <?php
                        $no = 1;
                        $tampil = $vp->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td> 
                    <td><?php echo $data->jns_modalitas; ?></td>
                    <td><?php echo $data->karakter; ?></td>  
                      <td align="center">
                        <a id="edit_profil" data-toggle="modal" data-target="#edit" data-idprof="<?php echo $data->id_profil; ?>" data-jnsm="<?php echo $data->jns_modalitas; ?>" data-k="<?php echo $data->karakter; ?>">
                        <button class="btn btn-info btn-xs" onclick="dttampildua('<?php echo $data->id_profil; ?>')"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                       
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Profil</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                                <input type="hidden" name="id_profil" id="id_profil">
                               <div class="form-group">
                                 <label class="control-label" for="jns_modalitas">Jenis Modalitas</label>
                                 <select name="jns_modalitas" class="form-control" >
                                    <option id="tampil_disini2"></option>
                                    <option value = "Visual">Visual</option>
                                    <option value = "Auditorial">Auditorial</option>
                                    <option value = "Read-Write">Read-Write</option>
                                    <option value = "Kinestetik">Kinestetik</option>
                                 </select>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="karakter">Karakter</label>
                                 <textarea name="karakter" class="form-control" id="karakter" required></textarea>
                                 
                              </div>
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_profil", function() {
                          var idprofil = $(this).data('idprof');                        
                          var jenismodalitas = $(this).data('jnsm');
                          var karakter = $(this).data('k');
                          $("#modal-edit #id_profil").val(idprofil);
                          $("#modal-edit #jns_modalitas").val(jenismodalitas);
                          $("#modal-edit #karakter").val(karakter);
                      });
                        
                      function dttampil(){
                          $(document).ready(function() {   
                            var data_table = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select.php",             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table += data[ii].option;
                                         }

                                        $('#tampil_disini').html(data_table);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }
                      function dttampildua(x){
                          $(document).ready(function() {   
                            var data_table2 = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select_4.php",
                                    data: {id:x},             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(x);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table2 += data[ii].option;
                                         }

                                        $('#tampil_disini2').html(data_table2);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }
                     

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_profil.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
              
                  