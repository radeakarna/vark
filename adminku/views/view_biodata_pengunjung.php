<?php
include "models/m_view_biodata_pengunjung.php";  

$vb = new Biodata($connection);

if(@$_GET['act'] == '') {
?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Biodata Pengunjung</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                      <tr>
                        <th>NO.</th>
                        <th>NIM</th>
                        <th>NAMA</th>
                        <th>JENIS KELAMIN</th>
                        <th>JURUSAN</th>
                        <th>OPSI</th>
                      </tr>
                    </thead>
                    
                      
                      <?php
                        $no = 1;
                        $tampil = $vb->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->nim; ?></td>
                    <td><?php echo $data->nama_pengunjung; ?></td>
                    <td><?php echo $data->jns_kelamin; ?></td>  
                    <td><?php echo $data->jurusan; ?></td>  
                      <td align="center">
                        <a id="edit_biodata_pengunjung" data-toggle="modal" data-target="#edit" data-nm="<?php echo $data->nim; ?>"  data-np="<?php echo $data->nama_pengunjung; ?>" data-jk="<?php echo $data->jns_kelamin; ?>" data-ju="<?php echo $data->jurusan; ?>">
                        <button class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_biodata_pengunjung&act=del&nim=<?php echo $data->nim; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                    <tfoot></tfoot>
                  </table>
                </div>
                  
                  
                  <botton type="button" class="btn btn-success" data-toggle="modal" onclick="dttampil()" data-target="#tambah"> Tambah Data</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Biodata Pengunjung</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                               <div class="form-group">
                                 <label class="control-label" for="nim">NIM</label>
                                 <input type="text" name="nim" class="form-control" id="nim" required>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="nama_pengunjung">Nama</label>
                                 <input type="text" name="nama_pengunjung" class="form-control" id="nama_pengunjung" required>
                              </div>

                             <div class="form-group">
                                 <label class="control-label" for="jns_kelamin">Jenis Kelamin</label>
                                  <select class="form-control" id="tampil_disini" name="tampil_jnskelamin">
                                    <option value="laki-laki">laki-laki</option>
                                     <option value="perempuan">perempuan</option>
                                  </select>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jurusan">Jurusan</label>
                                 <input type="text" name="jurusan" class="form-control" id="jurusan" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php
                                
                              if(isset($_POST['tambah'])){
                                $nim = $connection->conn->real_escape_string($_POST['nim']);
                                $nama_pengunjung = $connection->conn->real_escape_string($_POST['nama_pengunjung']);
                                $jns_kelamin = $connection->conn->real_escape_string($_POST['tampil_jnskelamin']);
                                $jurusan = $connection->conn->real_escape_string($_POST['jurusan']);
                                $vb->tambah($nim, $nama_pengunjung, $jns_kelamin, $jurusan);
                                //header("location:?page=view_modalitas");
                                  ?>
                                    <script>
                                        document.location='?page=view_biodata_pengunjung';
                                    </script>
                                <?php
                              }
                          ?>
                          
                      </div>  
                    </div>
                  </div>
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Biodata Pengunjung</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                              
                              <div class="form-group">
                                 <label class="control-label" for="nim">NIM</label>
                                 <input type="text" name="nim" class="form-control" id="nim" required>
                                 <input type="hidden" name="nim_lama" class="form-control" id="nim" required>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="nama_pengunjung">Nama</label>
                                 <input type="text" name="nama_pengunjung" class="form-control" id="nama_pengunjung" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jns_kelamin">Jenis Kelamin</label>
                                  <select class="form-control" id="tampil_disini" name="tampil_jnskelamin">
                                    <option value="laki-laki">laki-laki</option>
                                     <option value="perempuan">perempuan</option>
                                  </select>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jurusan">Jurusan</label>
                                 <input type="text" name="jurusan" class="form-control" id="jurusan" required>
                              </div>

                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_biodata_pengunjung", function() {
                          var nim = $(this).data('nm');
                          var namapengunjung = $(this).data('np');
                          var jnskelamin = $(this).data('jk');
                          var jurusan = $(this).data('ju');
                          $("#modal-edit #nim").val(nim);
                          $("#modal-edit #nama_pengunjung").val(namapengunjung);
                          $("#modal-edit #jns_kelamin").val(jnskelamin);
                          $("#modal-edit #jurusan").val(jurusan);
                      });
                        
                      function dttampil(){
                          $(document).ready(function() {   
                            var data_table = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select_jk.php",             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table += data[ii].option;
                                         }

                                        $('#tampil_disini').html(data_table);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_biodata_pengunjung.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $vb->hapus($_GET['nim']);
    //header("location:?page=view_modalitas");
     ?>
                                    <script>
                                        document.location='?page=view_biodata_pengunjung';
                                    </script>
                                <?php
}              
                  