
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <!--<?php
        //$aa = array('laki-laki','perempuan');
        //print_r($aa);

      ?>
          <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Chart Presentase Hasil Penentuan Modalitas</h3>
              <!-- page start-->
              <div class="tab-pane" id="chartjs">
                  <div class="row mt">
                      <div class="col-lg-12">
                          <div class="content-panel">
                              <h4></i> Sortir Hasil Berdasarkan Jurusan dan Jenis Kelamin </h4>
                              <form method="POST" action="index.php?page=chart">
                                <div class="form-group col-md-12">
                                  <select name="sort" class="form-control">
                                    <?php
                                      include"../script/koneksi.php";
                                      if (isset($_POST['kirim'])) {
                                        $sort = $_POST['sort'];
                                        $gender = $_POST['gender'];
                                      }else{
                                        $sort = "Informatika";  
                                        $gender = "laki-laki";
                                      }
                                      
                                      $data = mysqli_query($con,"SELECT jurusan FROM biodata_pengunjung GROUP BY jurusan");
                                      while ($t = mysqli_fetch_array($data)) {
                                    ?>
                                      <option <?php if($sort==$t['jurusan']) echo 'selected="selected"'; ?> value="<?php echo $t['jurusan']; ?>"><?php echo $t['jurusan']; ?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                </div>
                                <div class="form-group col-md-12">
                                  <select name="gender" class="form-control">
                                    <option <?php if($gender=="'laki-laki'") echo 'selected="selected"'; ?> value="'laki-laki'">laki-laki</option>
                                    <option <?php if($gender=="'perempuan'") echo 'selected="selected"'; ?> value="'perempuan'">perempuan</option>
                                    <option <?php if($gender=="'laki-laki','perempuan'") echo 'selected="selected"'; ?> value="'laki-laki','perempuan'">laki-laki dan perempuan</option>
                                  </select>
                                </div>
                                <div class="form-group col-md-11">
                                  <input type="submit" name="kirim" class="btn btn-primary" value="Sortir">
                                </div>
                              </form>                              
                              
                              <div class="panel-body text-center">
                                  <canvas id="doughnut" height="300" width="400"></canvas>
                              </div>
                              <?php
                                if (isset($_POST['kirim'])) {
                                    $sort = $_POST['sort'];
                                    $gender = $_POST['gender'];

                                    $v = 0;
                                    $a = 0;
                                    $r = 0;
                                    $k = 0;

                                    $va = 0;
                                    $vr = 0;
                                    $vk = 0;
                                    $ak = 0;
                                    $ar = 0;
                                    $rk = 0;

                                    $ark = 0;
                                    $vrk = 0;
                                    $vak = 0;
                                    $var = 0;
                                    $vark = 0;
                                    $data = $_POST['gender'];
                                    $sortdata = "($data)";
                                    $chart = mysqli_query($con,"SELECT *FROM biodata_pengunjung INNER JOIN hasil ON biodata_pengunjung.nim = hasil.nim WHERE jns_kelamin IN $sortdata AND  jurusan = '$sort' ");
                                    $jumlah_data = mysqli_num_rows($chart);
                                    while ($jmchart = mysqli_fetch_array($chart)) {
                                        $kecenderungan = $jmchart['kecenderungan'];
                                        if($kecenderungan=='Visual'){
                                          $v+=1;
                                        }else if($kecenderungan=='Auditorial'){
                                          $a+=1;
                                        }else if($kecenderungan=='Read/Write'){
                                          $r+=1;
                                        }else if($kecenderungan=='Kinestetik'){
                                          $k+=1;
                                        }else if($kecenderungan=='Auditorial-Visual'){
                                          $va+=1;
                                        }else if($kecenderungan=='Read/Write-Visual'){
                                          $vr+=1;
                                        }else if($kecenderungan=='Kinestetik-Visual'){
                                          $vk+=1;
                                        }else if($kecenderungan=='Auditorial-Kinestetik'){
                                          $ak+=1;
                                        }else if($kecenderungan=='Auditorial-Read/Write'){
                                          $ar+=1;
                                        }else if($kecenderungan=='Kinestetik-Read/Write'){
                                          $rk+=1;
                                        }else if($kecenderungan=='Auditorial-Kinestetik-Read/Write'){
                                          $ark+=1;
                                        }else if($kecenderungan=='Kinestetik-Read/Write-Visual'){
                                          $vrk+=1;
                                        }else if($kecenderungan=='Auditorial-Kinestetik-Visual'){
                                          $vak+=1;
                                        }else if($kecenderungan=='Auditorial-Read/Write-Visual'){
                                          $var+=1;
                                        }else if($kecenderungan=='Auditorial-Kinestetik-Read/Write-Visual'){
                                          $vark+=1;
                                        }else{

                                        }
                                    }
                                    $v = substr((($v/$jumlah_data)*(100/100)*100),0,5);
                                    $a = substr((($a/$jumlah_data)*(100/100)*100),0,5);
                                    $r = substr((($r/$jumlah_data)*(100/100)*100),0,5);
                                    $k = substr((($k/$jumlah_data)*(100/100)*100),0,5);

                                    $va = substr((($va/$jumlah_data)*(100/100)*100),0,5);
                                    $vr = substr((($vr/$jumlah_data)*(100/100)*100),0,5);
                                    $vk = substr((($vk/$jumlah_data)*(100/100)*100),0,5);
                                    $ak = substr((($ak/$jumlah_data)*(100/100)*100),0,5);
                                    $ar = substr((($ar/$jumlah_data)*(100/100)*100),0,5);
                                    $rk = substr((($rk/$jumlah_data)*(100/100)*100),0,5);

                                    $ark = substr((($ark/$jumlah_data)*(100/100)*100),0,5);
                                    $vrk = substr((($vrk/$jumlah_data)*(100/100)*100),0,5);
                                    $vak = substr((($vak/$jumlah_data)*(100/100)*100),0,5);
                                    $var = substr((($var/$jumlah_data)*(100/100)*100),0,5);
                                    $vark = substr((($vark/$jumlah_data)*(100/100)*100),0,5);
                              ?>
                                   
                              <div class="panel-body text-center">
                              <p><h3>Presentase Modalitas belajar <?php echo $sort; ?> untuk <?php $ccf = str_replace("'", "", $gender); echo str_replace(","," dan ", $ccf); ?></h3></p>
                                  <table width="80%" style="color: white;" class="table" border= 2px;>
                                  <tr>
                                    <td style="background-color: #e1da44; font-weight: 1000;font-size:16px; " width="30%">Visual</td>
                                    <!--<td style="background-color: #e1da44;" width="5%">:</td>-->
                                    <td style="background-color: #e1da44; font-weight: 1000;font-size:16px;" width="15%"><?php echo $v."%"; ?></td>

                                    <td style="background-color: #aecc5a;font-weight: 1000;font-size:16px;" width="30%">Auditorial</td>
                                    <!--<td style="background-color: #aecc5a;" width="5%">:</td>-->
                                    <td style="background-color: #aecc5a; font-weight: 1000;font-size:16px;" width="15%"><?php echo $a."%"; ?></td>
                                  </tr>

                                  <tr>
                                    <td style="background-color: #fda02e; font-weight: 1000;font-size:16px; " width="30%">Read/Write</td>
                                    <!--<td style="background-color: #fda02e;" width="5%">:</td>-->
                                    <td style="background-color: #fda02e; font-weight: 1000;font-size:16px;" width="15%"><?php echo $r."%"; ?></td>

                                    <td style="background-color: #ab1010;font-weight: 1000;font-size:16px;" width="30%">Kinestetik</td>
                                    <!--<td style="background-color: #ab1010;" width="5%">:</td>-->
                                    <td style="background-color: #ab1010; font-weight: 1000;font-size:16px;" width="15%"><?php echo $k."%"; ?></td>
                                  </tr>

                                  <tr>
                                    <td style="background-color: #7a2762; font-weight: 1000;font-size:16px; " width="30%">Auditorial-Visual</td>
                                    <!--<td style="background-color: #7a2762;" width="5%">:</td>-->
                                    <td style="background-color: #7a2762; font-weight: 1000;font-size:16px;" width="15%"><?php echo $va."%"; ?></td>

                                    <td style="background-color: #db5461;font-weight: 1000;font-size:16px;" width="30%">Read/Write-Visual</td>
                                    <!--<td style="background-color: #db5461;" width="5%">:</td>-->
                                    <td style="background-color: #db5461; font-weight: 1000;font-size:16px;" width="15%"><?php echo $vr."%"; ?></td>
                                  </tr>

                                   <tr>
                                    <td style="background-color: #698f3f; font-weight: 1000;font-size:16px; " width="30%">Kinestetik-Visual</td>
                                    <!--<td style="background-color: #698f3f;" width="5%">:</td>-->
                                    <td style="background-color: #698f3f; font-weight: 1000;font-size:16px;" width="15%"><?php echo $vk."%"; ?></td>

                                    <td style="background-color: #000000;font-weight: 1000;font-size:16px;" width="30%">Auditorial-Kinestetik</td>
                                    <!--<td style="background-color: #000000;" width="5%">:</td>-->
                                    <td style="background-color: #000000; font-weight: 1000;font-size:16px;" width="15%"><?php echo $ak."%"; ?></td>
                                  </tr>


                                   <tr>
                                    <td style="background-color: #e7decd; font-weight: 1000;font-size:16px; " width="30%">Auditorial-Read/Write</td>
                                    <!--<td style="background-color: #e7decd;" width="5%">:</td>-->
                                    <td style="background-color: #e7decd; font-weight: 1000;font-size:16px;" width="15%"><?php echo $ar."%"; ?></td>

                                    <td style="background-color: #804e49;font-weight: 1000;font-size:16px;" width="30%">Kinestetik-Read/Write</td>
                                    <!--<td style="background-color: #804e49;" width="5%">:</td>-->
                                    <td style="background-color: #804e49; font-weight: 1000;font-size:16px;" width="15%"><?php echo $rk."%"; ?></td>
                                  </tr>

                                  <tr>
                                    <td style="background-color: #80d8df; font-weight: 1000;font-size:16px; " width="30%">Auditorial-Kinestetik-Read/Write</td>
                                    <!--<td style="background-color: #80d8df;" width="5%">:</td>-->
                                    <td style="background-color: #80d8df; font-weight: 1000;font-size:16px;" width="15%"><?php echo $ark."%"; ?></td>

                                    <td style="background-color: #b5838d;font-weight: 1000;font-size:16px;" width="30%">Kinestetik-Read/Write-Visual</td>
                                    <!--<td style="background-color: #b5838d;" width="5%">:</td>-->
                                    <td style="background-color: #b5838d; font-weight: 1000;font-size:16px;" width="15%"><?php echo $vrk."%"; ?></td>
                                  </tr>

                                   <tr>
                                    <td style="background-color: #36006f; font-weight: 1000;font-size:16px; " width="30%">Auditorial-Kinestetik-Visual</td>
                                    <!--<td style="background-color: #36006f;" width="5%">:</td>-->
                                    <td style="background-color: #36006f; font-weight: 1000;font-size:16px;" width="15%"><?php echo $vak."%"; ?></td>

                                    <td style="background-color: #98b287;font-weight: 1000;font-size:16px;" width="30%">Auditorial-Read/Write-Visual</td>
                                    <!--<td style="background-color: #98b287;" width="5%">:</td>-->
                                    <td style="background-color: #98b287; font-weight: 1000;font-size:16px;" width="15%"><?php echo $var."%"; ?></td>
                                  </tr>


                                   <tr>
                                    <td style="background-color: #9a7257; font-weight: 1000;font-size:16px; " width="30%">Auditorial-Kinestetik-Read/Write-Visual</td>
                                    <!--<td style="background-color: #9a7257;" width="5%">:</td>-->
                                    <td style="background-color: #9a7257; font-weight: 1000;font-size:16px;" width="15%"><?php echo $vark."%"; ?></td>

                                    <td style="background-color: #9a7257;font-weight: 1000;font-size:16px;" width="30%"></td>
                                    <!--<td style="background-color: #9a7257;" width="5%"></td>-->
                                    <td style="background-color: #9a7257; font-weight: 1000;font-size:16px;" width="15%"></td>
                                  </tr>






                                  


                                 

                              </table>
                              </div>
                              
                            <?php
                              }      
                            ?>
                           

                              
                            </div>
                          </div>
                      </div>
  
                  </div>
                  
              </div>
              <!-- page end-->
          </section> 
              
    

