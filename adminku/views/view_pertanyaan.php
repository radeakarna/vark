<?php
include "models/m_view_pertanyaan.php";  

$vp = new Pertanyaan($connection);

if(@$_GET['act'] == '') {
?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Pertanyaan</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th>NO.</th>
                      <th>ID PERTANYAAN</th>
                      <th>DESKRIPSI PERTANYAAN</th>
                      <th>OPSI</th>
                    </tr>
                     </thead>
                      
                      <?php
                        $no = 1;
                        $tampil = $vp->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->id_pertanyaan; ?></td>
                    <td><?php echo $data->des_pertanyaan; ?></td>  
                      <td align="center">
                        <a id="edit_pertanyaan" data-toggle="modal" data-target="#edit" data-idp="<?php echo $data->id_pertanyaan; ?>" data-desp="<?php echo $data->des_pertanyaan; ?>">
                        <button class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_pertanyaan&act=del&id=<?php echo $data->id_pertanyaan; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  <botton type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah"> Tambah Data</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data Pertanyaan</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body"> 
                              <div class="form-group">
                                 <label class="control-label" for="id_pertanyaan">ID Pertanyaan</label>
                                  <input type="text" name="id_pertanyaan" class="form-control" id="id_pertanyaan" required>
                              </div>
                              <div class="form-group">
                                 <label class="control-label" for="des_pertanyaan">Deskripsi Pertanyaan</label>
                                 <input type="text" name="des_pertanyaan" class="form-control" id="des_pertanyaan" required>
                              </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                           </div>
                          </form>

                          <?php
                                
                              if(isset($_POST['tambah'])){
                                $id_pertanyaan = $connection->conn->real_escape_string($_POST['id_pertanyaan']);
                                $des_pertanyaan = $connection->conn->real_escape_string($_POST['des_pertanyaan']);
                                
                                $vp->tambah($id_pertanyaan, $des_pertanyaan);
                                //header("location:?page=view_pertanyaan");
                                  ?>
                                    <script>
                                        document.location='?page=view_pertanyaan';
                                    </script>
                                <?php
                              }
                          ?>
                          
                      </div>  
                    </div>
                  </div>
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Pertanyaan</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                              <div class="form-group">
                                 <label class="control-label" for="id_pertanyaan">ID Pertanyaan</label>
                                 <input type="text" name="id_pertanyaan" class="form-control" id="id_pertanyaan" required>
                              </div>
                               
                              <div class="form-group">
                                 <label class="control-label" for="des_pertanyaan">Deskripsi Pertanyaan</label>
                                 <input type="text" name="des_pertanyaan" class="form-control" id="des_pertanyaan" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_pertanyaan", function() {
                          var idpertanyaan = $(this).data('idp');
                          var deskripsipertanyaan = $(this).data('desp');
                          $("#modal-edit #id_pertanyaan").val(idpertanyaan);
                          $("#modal-edit #des_pertanyaan").val(deskripsipertanyaan);
                      });



                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_pertanyaan.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $vp->hapus($_GET['id']);
    //header("location:?page=view_pertanyaan");
     ?>
                                    <script>
                                        document.location='?page=view_pertanyaan';
                                    </script>
                                <?php
}              
                  