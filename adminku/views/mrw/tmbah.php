<?php
include "models/Modalmateri.php";
$vm = new Modalmateri($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Tambah materi auditorial</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Tambah data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Judul materi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="judul">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Pdf </label>
                    <div class="col-sm-10">
                        <input type="file" name="materi">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">No urut ditampilkan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="urutan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="tambah" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['tambah'])) {
    $id_sub_materi = $_GET['as'];
    $judul         = $connection->conn->real_escape_string($_POST['judul']);
    //$materi        = $connection->conn->real_escape_string($_POST['materi']);
    $urutan = $_POST['urutan'];

    $tanggal                = date("Y-m-d");
    $ekstensi_diperbolehkan = array('pdf');
    $nama                   = $_FILES['materi']['name'];
    $x                      = explode('.', $nama);
    $ekstensi               = strtolower(end($x));
    $ukuran                 = $_FILES['materi']['size'];
    $file_tmp               = $_FILES['materi']['tmp_name'];
    $jam                    = date("hisdmy");
    $nama_dua               = $jam . '.' . $ekstensi;

    if (in_array($ekstensi, $ekstensi_diperbolehkan) == true) {
        if ($ukuran < 10044070) { //10 mb
            move_uploaded_file($file_tmp, '../kelas/pdf/' . $nama_dua);
            $vm->tambah($id_sub_materi, $judul, $nama_dua, $urutan);
            echo "
            <script>
                alert('Data berhasil di inputkan');
                document.location='?page=mtambahrw&as=$id_sub_materi';
            </script>
            ";
        } else {
            echo '<script>alert("Ukuran file terlalu besar");</script>';
        }
    } else {
        echo '<script>alert("EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN");</script>';
    }
}?>