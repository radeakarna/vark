<?php
include "models/visual/Visual.php";
$vm = new Visual($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Data Sub Materi</span>
    <a href="index.php?page=tambahvisual"><button type="button" class="btn btn-default pull-right"> <i class="fa fa-plus"></i> Tambah Data</button></a>
    </div>
</div>

<div class="row mt">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="data_table">
            <thead>
            <tr>
                <th><center>NO.</center></th>
                <th><center>Nama materi</center></th>
                <th><center>Bab </center></th>
                <th><center>Opsi</center></th>
            </tr>
            </thead>
<?php
$no     = 1;
$tampil = $vm->tampil();
while ($data = $tampil->fetch_object()) {
    ?>
            <tr>
            <td align="center"><?php echo $no++ . "."; ?></td>
            <td><?php echo $data->nama_materi; ?></td>
            <td><?php echo $data->materi; ?></td>
            <td align="center">
                <a href="index.php?page=mvisual&as=<?php echo $data->sub_materi_id; ?>">
                <button class="btn btn-success btn-xs" data-toggle='tooltip' title='Isi materi'><i class="fa fa-plus"></i></button>
                </a>
                <a href="index.php?page=mpertanyaan&as=<?php echo $data->sub_materi_id; ?>">
                <button class="btn btn-warning btn-xs" data-toggle='tooltip' title='Tambah Pertanyaan'><i class="fa fa-question"></i></button>
                </a>
                <a href="index.php?page=editvisual&as=<?php echo $data->sub_materi_id; ?>">
                <button class="btn btn-info btn-xs" data-toggle='tooltip' title='Edit Data'><i class="fa fa-edit"></i></button>
                </a>
                <a href="?page=visual&act=del&id=<?php echo $data->sub_materi_id; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                <button class="btn btn-danger btn-xs" data-toggle='tooltip' title='Hapus Data'><i class="fa fa-trash-o"></i></button>
                </a>
            </td>
            </tr>
        <?php
}?>
            </table>
        </div>
    </div>
</div>
<?php
if (@$_GET['act'] == 'del') {
    $id_modalitas  = 1;
    $sub_materi_id = $_GET['id'];
    $vm->hapus($sub_materi_id);
    ?>
    <script>
        document.location='?page=visual';
    </script>
<?php
}?>