<?php
include "models/visual/Visual.php";
$vm = new Visual($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Data Sub Materi</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Tambah data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Sub Judul materi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="sub_materi">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama Materi</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="id_makul">
<?php
$no     = 1;
$tampil = $vm->getMakul('materi');
while ($data = $tampil->fetch_object()) {
    ?>
                            <option value="<?=$data->materi_id;?>"><?=$data->materi;?></option>
<?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="keterangan"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="tambah" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['tambah'])) {
    $id_modalitas  = 1;
    $id_materi     = $_POST['id_makul'];
    $jns_modalitas = $connection->conn->real_escape_string($_POST['sub_materi']);
    $keterangan    = $connection->conn->real_escape_string($_POST['keterangan']);
    $vm->tambah($id_modalitas, $id_materi, $jns_modalitas, $keterangan);
    ?>
    <script>
        alert('Data berhasil di inputkan');
        document.location='?page=tambahvisual';
    </script>
<?php }?>