<?php
error_reporting(0);
include "models/Modalpertanyaan.php";
$vm = new Modalpertanyaan($connection);
$pilihan_ganda = $_GET['pg'];
$pilihan_ganda == 'false' ? $valuePG = "&pg=false" : $valuePG = null ;
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Tambah pertanyaan visual</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Tambah data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Judul materi</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="pertanyaan"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Kunci Jawaban</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="kunci"></textarea>
                    </div>
                </div>
                <?php if ($pilihan_ganda != 'false') {
                    echo '
                    <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Pilihan Ganda</label>
                    <div class="col-sm-10">
                    <ol type="a">
                    <li><input type="text" name="pg[]"></li>
                    <li><input type="text" name="pg[]"></li>
                    <li><input type="text" name="pg[]"></li>
                    <li><input type="text" name="pg[]"></li>
                    </ol>
                    </div>
                </div>
                    ';
                } ?>
                
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="tambah" value="Simpan">
                    </div>
                </div>
                
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['tambah'])) {
    $id_sub_materi = $_GET['as'];
    $pertanyaan = $connection->conn->real_escape_string($_POST['pertanyaan']);
    $kunci = $connection->conn->real_escape_string($_POST['kunci']);
    $vm->tambah($id_sub_materi, $pertanyaan, $kunci);
    $lastId = $vm->getLastId();
    // echo $vm->getLastId();
    if ($pilihan_ganda != 'false') {
        foreach ($_POST['pg'] as $pg) {
            $vm->tambahPg($lastId,$pg);
        }
    }
    ?>
     <script>
        alert('Data berhasil di inputkan');
        document.location='?page=mpertanyaan&as=<?=$id_sub_materi?><?=$valuePG?>';
    </script>
<?php }?>