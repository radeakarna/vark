<?php
error_reporting(0);
include "models/Modalpertanyaan.php";
$vm = new Modalpertanyaan($connection);
$id = $_GET['id'];
$id_sub_materi = $_GET['as'];
$pertanyaan = $vm->getPertanyaan($id,$id_sub_materi);
$data = $pertanyaan->fetch_object();
$pg = $vm->getPilgan($id);
$id_pg = [];
$pilihan_ganda = $_GET['pg'];
$pilihan_ganda == 'false' ? $valuePG = "&pg=false" : $valuePG = null ;
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Tambah pertanyaan visual</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Tambah data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Pertanyaan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="pertanyaan"><?=$data->pertanyaan;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Kunci Jawaban</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="kunci"><?= $data->kunci; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Pilihan Ganda</label>
                    <div class="col-sm-10">
                    <ol type="a">
                    <?php while ($pil = $pg->fetch_object()) {
                        array_push($id_pg,$pil->id);
                        echo '<li><input type="text" name="pg[]" value="'.$pil->pilihan_ganda.'"></li><br>';
                    }; ?>                    
                    </ol>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="tambah" value="Simpan">
                    </div>
                </div>
                
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['tambah'])) {
    $id = $_GET['id'];
    $pertanyaan = $connection->conn->real_escape_string($_POST['pertanyaan']);
    $kunci = $connection->conn->real_escape_string($_POST['kunci']);
    $vm->edit($id, $pertanyaan, $kunci);
    if ($pilihan_ganda != 'false'){
        for ($i=0; $i < 4 ; $i++) { 
            $vm->editPg($id_pg[$i],$_POST['pg'][$i]);
        }
    }
    
    ?>
     <script>
        alert('Data berhasil di inputkan');
        document.location='?page=mpertanyaan&as=<?=$id_sub_materi?><?=$valuePG?>';
    </script>
<?php }?>