<?php
include "models/Modalmateri.php";
$vm = new Modalmateri($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Tambah materi visual</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Tambah data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Judul materi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="judul">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Frame video </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="materi"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">No urut ditampilkan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="urutan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="tambah" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['tambah'])) {
    $id_sub_materi = $_GET['as'];
    $judul         = $connection->conn->real_escape_string($_POST['judul']);
    $materi        = $connection->conn->real_escape_string($_POST['materi']);
    $urutan        = $_POST['urutan'];
    $vm->tambah($id_sub_materi, $judul, $materi, $urutan);
    ?>
    <script>
        alert('Data berhasil di inputkan');
        document.location='?page=mvisual&as=<?=$id_sub_materi?>';
    </script>
<?php }?>