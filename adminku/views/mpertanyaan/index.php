<?php
error_reporting(0);
include "models/Modalpertanyaan.php";
$vm = new Modalpertanyaan($connection);
$pilihan_ganda = $_GET['pg'];
$pilihan_ganda == 'false' ? $valuePG = "&pg=false" : $valuePG = null ;

?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Daftar Pertanyaan</span>
    <a href="index.php?page=mtambahpertanyaan&as=<?=$_GET['as']?><?=$valuePG?>"><button type="button" class="btn btn-default pull-right"> <i class="fa fa-plus"></i> Tambah Pertanyaan</button></a>
    </div>
</div>

<div class="row mt">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="data_table">
            <thead>
            <tr>
                <th><center>NO.</center></th>
                <th><center>Pertanyaan</center></th>
                <th><center>Kunci</center></th>
                <?php if ($pilihan_ganda != 'false'){
                    echo '<th><center>Pilihan Ganda</center></th>';
                 } ?>
                
                <th><center>Opsi</center></th>
            </tr>
            </thead>
<?php
$no     = 1;
$tampil = $vm->tampil("$_GET[as]");
while ($data = $tampil->fetch_object()) {
    
    $pg = $vm->pg($data->id);
    ?>
            <tr>
            <td align="center"><?php echo $no++ . "."; ?></td>
            <td><?php echo $data->pertanyaan; ?></td>
            <td><?php echo $data->kunci; ?></td>
            <?php if ($pilihan_ganda != 'false'){?>
                <td><ol type="a"><?php while ($pil = $pg->fetch_object()) {
                echo "<li>$pil->pilihan_ganda</li>";
            }; ?></ol></td>
            <?php } ?>
            
            <td align="center">
                <a href="index.php?page=meditpertanyaan&id=<?php echo $data->id; ?>&as=<?=$_GET['as']?><?=$valuePG?>">
                <button class="btn btn-info btn-xs" data-toggle='tooltip' title='Edit Data'><i class="fa fa-edit"></i></button>
                </a>
                <a href="?page=mpertanyaan&act=del&id=<?php echo $data->id; ?>&as=<?=$_GET['as'];?><?=$valuePG?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                <button class="btn btn-danger btn-xs" data-toggle='tooltip' title='Hapus Data'><i class="fa fa-trash-o"></i></button>
                </a>
            </td>
            </tr>
        <?php
}?>
            </table>
        </div>
    </div>
</div>
<?php
if (@$_GET['act'] == 'del') {
    $pertanyaan_id = $_GET['id'];
    $as = $_GET['as'];
    $vm->hapus($pertanyaan_id,$as);
    ?>
    <script>
        document.location='?page=mpertanyaan&as=<?=$as?><?=$valuePG?>';
    </script>
<?php }?>