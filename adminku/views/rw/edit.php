<?php
include "models/visual/Visual.php";
$vm        = new Visual($connection);
$id        = $_GET['as'];
$getMateri = $vm->getSmateri('sub_materi', $id);
$r         = $getMateri->fetch_object();
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Data Sub Materi</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Edit data</div>
        <div class="panel-body">
            <form class="form-horizontal style-form" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Sub Judul materi</label>
                    <div class="col-sm-10">
                        <input type="hidden" class="form-control" name="sub_materi_id" value="<?=$r->sub_materi_id;?>">
                        <input type="text" class="form-control" name="sub_materi" value="<?=$r->nama_materi;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama Materi</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="id_makul">
<?php
$no     = 1;
$tampil = $vm->getMakul('materi');
while ($data = $tampil->fetch_object()) {
    if ($data->materi_id == $r->materi_id) {
        ?>
                            <option value="<?=$data->materi_id;?>" selected><?=$data->materi;?></option>
<?php
} else {
        ?>
                            <option value="<?=$data->materi_id;?>"><?=$data->materi;?></option>
<?php
}
    ?>
<?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="keterangan"><?=$r->keterangan;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="submit"class="btn btn-success" class="form-control" name="edit" value="Edit data">
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['edit'])) {
    $id_modalitas  = 4;
    $id_materi     = $_POST['id_makul'];
    $sub_materi_id = $_POST['sub_materi_id'];
    $jns_modalitas = $connection->conn->real_escape_string($_POST['sub_materi']);
    $keterangan    = $connection->conn->real_escape_string($_POST['keterangan']);
    $sql           = "UPDATE sub_materi SET materi_id = '$id_materi', id_solusi = '$id_modalitas', nama_materi='$jns_modalitas', keterangan='$keterangan' WHERE sub_materi_id = '$sub_materi_id'";
    $vm->edit($sql);
    ?>
    <script>
        alert('Data berhasil di update');
        document.location='?page=rw';
    </script>
<?php }?>