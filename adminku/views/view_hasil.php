<?php
include "models/m_view_hasil.php";  

$vh = new Hasil($connection);

if(@$_GET['act'] == '') {
?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Hasil</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th>NO.</th>
                      <th>NIM</th>
                      <th>Nama</th>
                      <th>VISUAL</th>
                      <th>AUDITORIAL</th>
                      <th>READ-WRITE</th>
                      <th>KINESTETIK</th>
                      <th>KECENDERUNGAN</th>
                      <th>OPSI</th>
                    </tr>
                    </thead>
                      
                      <?php
                        $no = 1;
                        $tampil = $vh->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->nim; ?></td>
                    <td><?php echo $data->nama_pengunjung ; ?></td>
                    <td><?php echo $data->jml_visual; ?></td>  
                    <td><?php echo $data->jml_auditorial; ?></td>  
                    <td><?php echo $data->jml_readwrite; ?></td>  
                    <td><?php echo $data->jml_kinestetik; ?></td>   
                    <td><?php echo $data->kecenderungan; ?></td>  
                      <td align="center">
                        <a id="edit_hasil" data-toggle="modal" data-target="#edit" data-nm="<?php echo $data->nim; ?>"   data-jv="<?php echo $data->jml_visual; ?>" data-ja="<?php echo $data->jml_auditorial; ?>" data-jrw="<?php echo $data->jml_readwrite; ?>" data-jki="<?php echo $data->jml_kinestetik; ?>" data-kcd="<?php echo $data->kecenderungan; ?>">
                        <button class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_hasil&act=del&nim=<?php echo $data->nim; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data hasil</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                              
                              <div class="form-group">
                                 <label class="control-label" for="nim">NIM</label>
                                 <input type="text" name="nim" class="form-control" id="nim" required>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="jml_visual">Visual</label>
                                 <input type="text" name="jml_visual" class="form-control" id="jml_visual" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jml_auditorial">Auditorial</label>
                                 <input type="text" name="jml_auditorial" class="form-control" id="jml_auditorial" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jml_readwrite">Read-Write</label>
                                 <input type="text" name="jml_readwrite" class="form-control" id="jml_readwrite" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="jml_kinestetik">Kinestetik</label>
                                 <input type="text" name="jml_kinestetik" class="form-control" id="jml_kinestetik" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="kecenderungan">Kecenderungan</label>
                                 <input type="text" name="kecenderungan" class="form-control" id="kecenderungan" required>
                              </div>

                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_hasil", function() {
                          var nim = $(this).data('nm');
                          var jml_visual = $(this).data('jv');
                          var jml_auditorial = $(this).data('ja');
                          var jml_readwrite = $(this).data('jrw');
                          var jml_kinestetik = $(this).data('jki');
                          var kecenderungan = $(this).data('kcd');
                          $("#modal-edit #nim").val(nim);
                          $("#modal-edit #jml_visual").val(jml_visual);
                          $("#modal-edit #jml_auditorial").val(jml_auditorial);
                          $("#modal-edit #jml_readwrite").val(jml_readwrite);
                          $("#modal-edit #jml_kinestetik").val(jml_kinestetik);
                          $("#modal-edit #kecenderungan").val(kecenderungan);
                      });
                        
                      

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_hasil.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $vh->hapus($_GET['nim']);
    //header("location:?page=view_modalitas");
     ?>
                                    <script>
                                        document.location='?page=view_hasil';
                                    </script>
                                <?php
}              
                  