<?php
include "models/m_view_solusi.php";  

$vs = new Solusi($connection);

if(@$_GET['act'] == '') {
?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Solusi</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th>NO.</th>
                      <th>ID SOLUSI</th>
                      <th>JENIS MODALITAS</th>
                      <th>INTAKE</th>
                      <th>OUTPUT</th>
                      <th>SWOT</th>
                      <th>OPSI</th>
                    </tr>
                    </thead>
                      
                      <?php
                        $no = 1;
                        $tampil = $vs->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->id_solusi; ?></td>
                    <td><?php echo $data->jns_modalitas; ?></td>
                    <td><?php echo $data->intake; ?></td>  
                    <td><?php echo $data->output; ?></td>
                    <td><?php echo $data->swot; ?></td>
                      <td align="center">
                        <a id="edit_solusi" data-toggle="modal" data-target="#edit" data-ids="<?php echo $data->id_solusi; ?>"  data-jnsm="<?php echo $data->jns_modalitas; ?>" data-itk="<?php echo $data->intake; ?>" data-otp="<?php echo $data->output; ?>" data-swt="<?php echo $data->swot; ?>">
                        <button class="btn btn-info btn-xs" onclick="dttampildua('<?php echo $data->id_solusi; ?>')"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_solusi&act=del&id=<?php echo $data->id_solusi; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  <botton type="button" class="btn btn-success" data-toggle="modal" onclick="dttampil()" data-target="#tambah"> Tambah Data</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data Solusi</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                               <div class="form-group">
                                 <label class="control-label" for="id_solusi">ID Solusi</label>
                                 <input type="text" name="id_solusi" class="form-control" id="id_solusi" required>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="jns_modalitas">Jenis Modalitas</label>
                                 <select name="jns_modalitas" class="form-control" >
                                    <option id="tampil_disini2"></option>
                                    <option value = "Visual">Visual</option>
                                    <option value = "Auditorial">Auditorial</option>
                                    <option value = "Read-Write">Read-Write</option>
                                    <option value = "Kinestetik">Kinestetik</option>
                                 </select>
                              </div>  

                              <div class="form-group">
                                 <label class="control-label" for="intake">Intake</label>
                                 <textarea name="intake" class="form-control" id="intake"></textarea>
                                 
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="output">Output</label>
                                 <input type="text" name="output" class="form-control" id="output" required>
                              </div>

                               <div class="form-group">
                                 <label class="control-label" for="swot">Swot</label>
                                 <input type="text" name="swot" class="form-control" id="swot" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php
                                
                              if(isset($_POST['tambah'])){
                                $id_solusi = $connection->conn->real_escape_string($_POST['id_solusi']);
                                $jns_modalitas = $connection->conn->real_escape_string($_POST['tampil_jnsmodalitas']);
                                $intake = $connection->conn->real_escape_string($_POST['intake']);
                                $output = $connection->conn->real_escape_string($_POST['output']);
                                $swot = $connection->conn->real_escape_string($_POST['swot']);

                                $vs->tambah($id_solusi, $jns_modalitas, $intake, $output, $swot);
                                //header("location:?page=view_modalitas");
                                  ?>
                                    <script>
                                        document.location='?page=view_solusi';
                                    </script>
                                <?php
                              }
                          ?>
                          
                      </div>  
                    </div>
                  </div>
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Solusi</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                               <div class="form-group">
                                 <label class="control-label" for="id_solusi">ID Solusi</label>
                                 <input type="text" name="id_solusi" class="form-control" id="id_solusi" required>
                              </div>  

                               <div class="form-group">
                                 <label class="control-label" for="jns_modalitas">Jenis Modalitas</label>
                                 <select name="jns_modalitas" class="form-control" >
                                    <option id="tampil_disini3"></option>
                                    <option value = "Visual">Visual</option>
                                    <option value = "Auditorial">Auditorial</option>
                                    <option value = "Read-Write">Read-Write</option>
                                    <option value = "Kinestetik">Kinestetik</option>
                                 </select>
                              </div>   

                               <div class="form-group">
                                 <label class="control-label" for="intake">Intake</label>
                                 <textarea name="intake" class="form-control" id="intake" required></textarea>  
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="output">Output</label>
                                 <input type="text" name="output" class="form-control" id="output" required>
                              </div>

                               <div class="form-group">
                                 <label class="control-label" for="swot">Swot</label>
                                 <input type="text" name="swot" class="form-control" id="swot" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_solusi", function() {
                          var idsolusi = $(this).data('ids');
                          var jenismodalitas = $(this).data('jnsm');
                          var intake = $(this).data('itk');
                          var output = $(this).data('otp');
                          var swot = $(this).data('swt');
                          $("#modal-edit #id_solusi").val(idsolusi);
                          $("#modal-edit #jns_modalitas").val(jenismodalitas);
                          $("#modal-edit #intake").val(intake);
                          $("#modal-edit #output").val(output);
                          $("#modal-edit #swot").val(swot);
                      });
                        
                      function dttampil(){
                          $(document).ready(function() {   
                            var data_table = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select_modalitas.php",             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table += data[ii].option;
                                         }

                                        $('#tampil_disini').html(data_table);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }


                      function dttampildua(x){
                        $(document).ready(function() {   
                          var data_table2 = "";             
                                $.ajax({    //create an ajax request to load_page.php
                                  type: "POST",
                                  url: "views/select_3.php",
                                  data: {id:x},             
                                  dataType: "json",   //expect html to be returned                
                                  success: function(data){                    
                                      //$("#sasal").html(response); 
                                      //alert(response);
                                      for (var ii = 0; ii <data.length; ii++) {
                                           data_table2 += data[ii].option;
                                       }

                                      $('#tampil_disini3').html(data_table2);
                                      //document.getElementById("sasal").innerHTML = x;

                                  }

                              });
                          });

                      }

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_solusi.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $vs->hapus($_GET['id']);
    //header("location:?page=view_modalitas");
     ?>
                                    <script>
                                        document.location='?page=view_solusi';
                                    </script>
                                <?php
}              
                  