<?php
include "models/Modalmateri.php";
$vm        = new Modalmateri($connection);
$as        = $_GET['as'];
$id        = $_GET['id'];
$getMateri = $vm->getSmateri('main_materi', $id);
$r         = $getMateri->fetch_object();
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Materi Visual</span>
    </div>
</div>
<div class="row mt">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-tasks"></i> Edit data</div>
        <div class="panel-body">
        <form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Judul materi</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="judul" value="<?=$r->materi?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label"> </label>
                <div class="col-sm-10">
                <audio controls>
                    <source src="../kelas/audio/<?=$r->isi_materi?>" type="audio/mpeg" controls>
                </audio>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Audio (mp3/ ogg/ mpeg) </label>
                <div class="col-sm-10">
                    <input type="file" name="materi">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">No urut ditampilkan</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="urutan" value="<?=$r->no_urut?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label"></label>
                <div class="col-sm-10">
                    <input type="submit"class="btn btn-success" class="form-control" name="edit" value="Edit data">
                </div>
            </div>
        </form>
        </div>
        </div>

    </div>
</div>
<?php
if (isset($_POST['edit'])) {
    $judul  = $connection->conn->real_escape_string($_POST['judul']);
    $urutan = $_POST['urutan'];

    $tanggal                = date("Y-m-d");
    $ekstensi_diperbolehkan = array('ogg', 'mpeg', 'mp3');
    $nama                   = $_FILES['materi']['name'];
    $x                      = explode('.', $nama);
    $ekstensi               = strtolower(end($x));
    $ukuran                 = $_FILES['materi']['size'];
    $file_tmp               = $_FILES['materi']['tmp_name'];
    $jam                    = date("hisdmy");
    $nama_dua               = $jam . '.' . $ekstensi;
    if ($nama) {
        if (in_array($ekstensi, $ekstensi_diperbolehkan) == true) {
            if ($ukuran < 10044070) { //10 mb
                //upload file baru
                move_uploaded_file($file_tmp, '../kelas/audio/' . $nama_dua);
                //hapus data lama
                $file = $r->isi_materi;
                unlink("../kelas/audio/" . $file);
                //Simpan data databasw
                $sql = "UPDATE main_materi SET materi = '$judul', isi_materi = '$nama_dua', no_urut = '$urutan' WHERE main_materi_id = '$id'";
                $vm->edit($sql);
                echo "
                <script>
                    alert('Data berhasil di ubah');
                    document.location='?page=mauditorial&as=$as';
                </script>
                ";
            } else {
                echo "<script>alert('Ukuran file terlalu besar');
                document.location='?page=mauditorial&as=$as';
                </script>
                ";
            }
        } else {
            echo "<script>
            alert('EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN');
            document.location='?page=mauditorial&as=$as';
            </script>";
        }
    } else {
        $sql = "UPDATE main_materi SET materi = '$judul', no_urut = '$urutan' WHERE main_materi_id = '$id'";
        $vm->edit($sql);
        echo "
        <script>
            alert('Data berhasil di ubah');
            document.location='?page=mauditorial&as=$as';
        </script>";
    }
}?>