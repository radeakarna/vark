<?php
include "models/Modalmateri.php";
$vm = new Modalmateri($connection);
?>
<div class="row mt">
    <div class="col-lg-12">
    <i class="fa fa-angle-right"></i> <span style="">Materi Auditorial</span>
    <a href="index.php?page=mtambahauditorial&as=<?=$_GET['as']?>"><button type="button" class="btn btn-default pull-right"> <i class="fa fa-plus"></i> Tambah Data</button></a>
    </div>
</div>

<div class="row mt">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="data_table">
            <thead>
            <tr>
                <th><center>NO.</center></th>
                <th><center>Nama materi</center></th>
                <th><center>Materi</center></th>
                <th><center>Opsi</center></th>
            </tr>
            </thead>
<?php
$no     = 1;
$tampil = $vm->Ttampil('3', "$_GET[as]");
while ($data = $tampil->fetch_object()) {
    ?>
            <tr>
            <td align="center"><?php echo $no++ . "."; ?></td>
            <td><?php echo $data->materi; ?></td>
            <td>
                <audio controls>
                    <source src="../kelas/audio/<?=$data->isi_materi?>" type="audio/mpeg" controls>
                </audio>
            </td>
            <td align="center">
                <a href="index.php?page=meditauditorial&id=<?php echo $data->main_materi_id; ?>&as=<?=$_GET['as']?> ">
                <button class="btn btn-info btn-xs" data-toggle='tooltip' title='Edit Data'><i class="fa fa-edit"></i></button>
                </a>
                <a href="?page=mauditorial&act=del&id=<?php echo $data->main_materi_id; ?>&as=<?=$_GET['as'];?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                <button class="btn btn-danger btn-xs" data-toggle='tooltip' title='Hapus Data'><i class="fa fa-trash-o"></i></button>
                </a>
            </td>
            </tr>
        <?php
}?>
            </table>
        </div>
    </div>
</div>
<?php
if (@$_GET['act'] == 'del') {
    $as             = $_GET['as'];
    $main_materi_id = $_GET['id'];
    $getMateri      = $vm->getSmateri('main_materi', $main_materi_id);
    $r              = $getMateri->fetch_object();
    $file           = $r->isi_materi;

    $vm->hapus($main_materi_id);
    unlink("../kelas/audio/" . $file);
    ?>
    <script>
        document.location='?page=mauditorial&as=<?=$as?>';
    </script>
<?php }?>