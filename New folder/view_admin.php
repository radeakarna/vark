<?php
include "models/m_view_admin.php";  

$va = new Admin($connection);

if(@$_GET['act'] == '') {
?>



            <div class="row mt">
              <div class="col-lg-12">
              <h3><i class="fa fa-angle-right"></i> Data Admin</h3>
             
              </div>
            </div>
      
            <div class="row mt">
              <div class="col-lg-12">
             
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="data_table">
                    <thead>
                    <tr>
                      <th><center>NO.</center></th>
                      <th><center>NAMA ADMIN</center></th>
                      <th><center>USERNAME</center></th>
                      <th><center>PASSWORD</center></th>
                      <th><center>OPSI</center></th>
                    </tr>
                    </thead>
                      <?php
                        $no = 1;
                        $tampil = $va->tampil();
                        while($data=$tampil->fetch_object()) {
                      ?>

                    <tr>
                    <td align="center"><?php echo $no++."."; ?></td>
                    <td><?php echo $data->nama_admin; ?></td> 
                    <td><?php echo $data->username; ?></td>
                    <td><?php echo $data->password; ?></td>  
                      <td align="center">
                        <a id="edit_admin" data-toggle="modal" data-target="#edit" data-na="<?php echo $data->nama_admin; ?>" data-us="<?php echo $data->username; ?>" data-pass="<?php echo $data->password; ?>" data-id="<?php echo $data->id_admin; ?>">
                        <button class="btn btn-info btn-xs" onclick="dttampildua('<?php echo $data->id_admin; ?>')"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        <a href="?page=view_admin&act=del&id=<?php echo $data->id_admin; ?>" onclick="return confirm('Yakin anda ingin menghapus data ini?')">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button> 
                        </a>
                      </td>
                    </tr> 
                      <?php
                        } ?>
                  </table>
                </div>
                  
                  
                  <botton type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah"> Tambah Data Admin</botton>
                  <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Data Admin</h4>
                        </div>
                          <form action="" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                              <div class="form-group">
                                 <label class="control-label" for="nama_admin">Nama Admin</label>
                                 <input type="text" name="nama_admin" class="form-control" id="nama_admin" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="username">Username</label>
                                 <input type="text" name="username" class="form-control" id="username" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="password">Password</label>
                                 <input type="text" name="password" class="form-control" id="password" required>
                              </div>

                            </div>

                            <div class="modal-footer">
                               <button type="reset" class="btn btn-danger">Reset</button>
                               <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                            </div>
                          </form>

                          <?php
                                
                              if(isset($_POST['tambah'])){
                               
                                $nama_admin = $connection->conn->real_escape_string($_POST['nama_admin']);
                                $username = $connection->conn->real_escape_string($_POST['username']);
                                $password = $connection->conn->real_escape_string($_POST['password']);
                                
                                $va->tambah($nama_admin, $username, $password);
                                //header("location:?page=view_modalitas");
                                  ?>
                                    <script>
                                        document.location='?page=view_admin';
                                    </script>
                                <?php
                              }
                          ?>
                          
                      </div>  
                    </div>
                  </div>
                  
                  <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Admin</h4>
                        </div>
                          <form id="form" enctype="multipart/form-data">
                            <div class="modal-body" id="modal-edit">
                              <div class="form-group">
                                 <label class="control-label" for="nama_admin">Nama Admin</label>
                                 <input type="text" name="nama_admin" class="form-control" id="nama_admin" required>
                                 <input type="hidden" name="id_admin" class="form-control" id="id_admin" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="username">Username</label>
                                 <input type="text" name="username" class="form-control" id="username" required>
                              </div>

                              <div class="form-group">
                                 <label class="control-label" for="password">Password</label>
                                 <input type="text" name="password" class="form-control" id="password" required>
                              </div> 
                            </div>
                            <div class="modal-footer">
                               <input type="submit" class="btn btn-success" name="edit" value="Simpan">
                            </div>
                          </form>   
                      </div>  
                    </div>
                      <script src="assets/assets/js/jquery.js"></script>
                    <script type="text/javascript">
                      $(document).on("click", "#edit_admin", function() {
                          var namaadmin = $(this).data('na');
                          var username = $(this).data('us');
                          var password = $(this).data('pass');
                          var id_admin = $(this).data('id');
                          
                          $("#modal-edit #nama_admin").val(namaadmin);
                          $("#modal-edit #username").val(username);
                          $("#modal-edit #password").val(password);
                          $("#modal-edit #id_admin").val(id_admin);
                      });
                        
                    


                      function dttampildua(x){
                          $(document).ready(function() {   
                            var data_table2 = "";             
                                  $.ajax({    //create an ajax request to load_page.php
                                    type: "POST",
                                    url: "views/select_5.php",
                                    data: {id:x},             
                                    dataType: "json",   //expect html to be returned                
                                    success: function(data){                    
                                        //$("#sasal").html(response); 
                                        //alert(response);
                                        for (var ii = 0; ii <data.length; ii++) {
                                             data_table2 += data[ii].option;
                                         }

                                        $('#tampil_disini2').html(data_table2);
                                        //document.getElementById("sasal").innerHTML = x;

                                    }

                                });
                            });

                      }

                      $(document).ready(function(e) {
                          $("#form").on("submit", (function(e) {
                              e.preventDefault();
                              $.ajax({
                                  url : 'models/proses_edit_admin.php',
                                  type : 'POST',
                                  data : new FormData(this),
                                  contentType : false,
                                  cache : false,
                                  processData : false,
                                  success : function(msg) {
                                      $('.table').html(msg);
                                  }
                              });
                          }));
                      })
                  </script>  
                  </div>
                  
                </div>
             </div>
                  
<?php
}else if(@$_GET['act'] == 'del') {
    $va->hapus($_GET['id']);
    //header("location:?page=view_modalitas");
     ?>
                                    <script>
                                        document.location='?page=view_admin';
                                    </script>
                                <?php
}              
                  