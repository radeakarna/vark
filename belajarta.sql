-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2018 at 04:07 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajarta`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminku`
--

CREATE TABLE `adminku` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminku`
--

INSERT INTO `adminku` (`id_admin`, `nama_admin`, `username`, `password`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `biodata_pengunjung`
--

CREATE TABLE `biodata_pengunjung` (
  `nim` varchar(225) NOT NULL,
  `nama_pengunjung` varchar(225) NOT NULL,
  `jns_kelamin` varchar(225) NOT NULL,
  `jurusan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biodata_pengunjung`
--

INSERT INTO `biodata_pengunjung` (`nim`, `nama_pengunjung`, `jns_kelamin`, `jurusan`) VALUES
('L100140049', 'FAISAL ABDUL M', 'laki-laki', 'komunikasi'),
('L100140097', 'FAURONI AKHSAN', 'laki-laki', 'komunikasi'),
('L100140107', 'MUHAMMAD CHOIRUL UMAM', 'laki-laki', 'komunikasi'),
('L100140110', 'MUHAMMAD ROMADANA', 'laki-laki', 'komunikasi'),
('L100140112', 'FARAH NUR AZIZAH', 'perempuan', 'komunikasi'),
('L100140113', 'ELSA VIONITA', 'perempuan', 'komunikasi'),
('L100140129', 'ALVAN LAZUARDIE ALKHAF', 'laki-laki', 'komunikasi'),
('L100144016', 'IMA DESTYA', 'perempuan', 'komunikasi'),
('L100144018', 'RINI KURNIA UTAMI', 'perempuan', 'komunikasi'),
('L100144020', 'CHANIFATUNNISA', 'perempuan', 'komunikasi'),
('L100150024', 'AMAR NUR FADHILA', 'laki-laki', 'komunikasi'),
('L100150080', 'MUHAMMAD RIDWAN', 'laki-laki', 'komunikasi'),
('L100150083', 'NAF''AN KAMIL', 'laki-laki', 'komunikasi'),
('L100150099', 'ANISA MAHARANI DETI FADHILAH', 'perempuan', 'komunikasi'),
('L100150105', 'DYAH NR', 'perempuan', 'komunikasi'),
('L100150115', 'ESTI JULIA MAULITIKA', 'perempuan', 'komunikasi'),
('L100150117', 'DYAH AYUNINGTYAS', 'perempuan', 'komunikasi'),
('L100150118', 'SAKINA IZZANI MAULANI', 'perempuan', 'komunikasi'),
('L100150120', 'RIZKY YUDA PRATAMA', 'laki-laki', 'komunikasi'),
('L100150125', 'MUHAMMAD ISA', 'laki-laki', 'komunikasi'),
('L100150127', 'NADA RIZKI EKA MAHARANI', 'perempuan', 'komunikasi'),
('L100150129', 'ANDRE YUDHA A', 'laki-laki', 'komunikasi'),
('L100150132', 'MUH. YUSUF RUHBANA', 'laki-laki', 'komunikasi'),
('L100150138', 'MUHAMMAD FEBRI WISNU A', 'laki-laki', 'komunikasi'),
('L100150140', 'BIMO S', 'laki-laki', 'komunikasi'),
('L100150148', 'MUH. HUSAIN M', 'laki-laki', 'komunikasi'),
('L100150152', 'NICO TUMURTA', 'laki-laki', 'komunikasi'),
('L100160002', 'DEVINA HASRILIA PUTRI', 'perempuan', 'komunikasi'),
('L100160007', 'REDHA YUANITA PERDANA', 'perempuan', 'komunikasi'),
('L100160032', 'ATIKA RACHMAWATI', 'perempuan', 'komunikasi'),
('L100160059', 'SALMA PUTRI WIJAYANTI', 'perempuan', 'komunikasi'),
('L100160088', 'ADAM PANJI PRIBADI', 'laki-laki', 'komunikasi'),
('L100160117', 'GALIH KUSMIARA', 'laki-laki', 'komunikasi'),
('L100160122', 'DEWI MONICA JULLIANA', 'perempuan', 'komunikasi'),
('L100160123', 'HENDRA JENSI UTOMO', 'laki-laki', 'komunikasi'),
('L100160124', 'MIRANDA AININ', 'perempuan', 'komunikasi'),
('L100160125', 'PHILIPS NUR JAMIL', 'laki-laki', 'komunikasi'),
('L100160137', 'AMALIA RAMADHANI', 'perempuan', 'komunikasi'),
('L100160142', 'APRILIA AS', 'perempuan', 'komunikasi'),
('L100160167', 'RISKA SEPRIANI A', 'perempuan', 'komunikasi'),
('L100170006', 'M. GILANG R', 'laki-laki', 'komunikasi'),
('L100170019', 'IMANDA ALVIN DWI ARYA', 'laki-laki', 'komunikasi'),
('L100170029', 'TIGOR NUR ROHMAN', 'laki-laki', 'komunikasi'),
('L100170030', 'TIKO ABDUL ROHMAN W', 'laki-laki', 'komunikasi'),
('L100170048', 'AININ WILDAN WALIDAINI', 'perempuan', 'komunikasi'),
('L100174024', 'AFIFAH KUSUMA NIGRUM', 'perempuan', 'komunikasi'),
('L100174112', 'UFIA MAULA KAMAL', 'perempuan', 'komunikasi'),
('L101173007', 'YUNI ANNAFI', 'perempuan', 'komunikasi'),
('L101173009', 'ARSY FITRIA KUSUMAWARDHANI', 'perempuan', 'komunikasi'),
('L200140002', 'Anggit', 'perempuan', 'Informatika'),
('L200140008', 'DIMAS STYADI', 'laki-laki', 'Informatika'),
('L200140100', 'SAMSUDIN NUR HIDAYAH', 'laki-laki', 'komunikasi'),
('L200150093', 'YUNITA ARDIYANTO', 'laki-laki', 'Informatika'),
('L200150117', 'WIDIYARTI ENDANG SAPUTRI', 'perempuan', 'Informatika'),
('L200150123', 'DANINDYA PUPUT MULIANA PUTRI', 'perempuan', 'Informatika'),
('L200154010', 'RAFIQA MAHARANI PUTRI SIREGAR', 'perempuan', 'Informatika'),
('L200160007', 'FARIS ZAKY ALFAIT', 'laki-laki', 'Informatika'),
('L200160008', 'FAFAH HANIFAH', 'perempuan', 'Informatika'),
('L200160009', 'WILDAN AUDINA', 'perempuan', 'Informatika'),
('L200160010', 'DENIAS RISMA PUTRI', 'perempuan', 'Informatika'),
('L200160013', 'REGITA CAHYA PRAMESTHI', 'perempuan', 'Informatika'),
('L200160014', 'AGESTA AKBAR BUDIHARTO', 'laki-laki', 'Informatika'),
('L200160015', 'AULIA RACHMAWATI', 'perempuan', 'Informatika'),
('L200160017', 'DINAYASINTA', 'perempuan', 'Informatika'),
('L200160018', 'ZUHDI FATHURRAHMAN', 'laki-laki', 'Informatika'),
('L200160022', 'MARTHA AMALIA KUSUMANINGRUM', 'perempuan', 'Informatika'),
('L200160024', 'ILHAM A.S', 'laki-laki', 'Informatika'),
('L200160031', 'TIAS NUR AINI', 'perempuan', 'Informatika'),
('L200160039', 'NOVRI NOVITA SARI', 'perempuan', 'Informatika'),
('L200160079', 'RIZKY SEPTIANA', 'perempuan', 'Informatika'),
('L200160082', 'DINDHA A.R', 'perempuan', 'Informatika'),
('L200160105', 'WILDAN FEBRIAN', 'laki-laki', 'Informatika'),
('L200160106', 'SANDI NUR ALAM', 'laki-laki', 'Informatika'),
('L200160107', 'RAHAYUNING PUTRI MAHARDIKAWATI', 'perempuan', 'Informatika'),
('L200160112', 'HENI HANIFAH', 'perempuan', 'Informatika'),
('L200160113', 'AHMAT CHOLID', 'laki-laki', 'Informatika'),
('L200160115', 'GALIH DWI CAKSONO', 'laki-laki', 'Informatika'),
('L200160119', 'INO PUSPASARI', 'perempuan', 'Informatika'),
('L200160120', 'MOH. AWALUDDIN HABIBIE', 'laki-laki', 'Informatika'),
('L200160122', 'REYHANNISA ERICO DWI RAMADHANA', 'laki-laki', 'Informatika'),
('L200160125', 'RONI SETYAWAN', 'laki-laki', 'Informatika'),
('L200160126', 'MUHAMMAD LUTHFI ', 'laki-laki', 'Informatika'),
('L200160128', 'SALWA SHOFIA', 'perempuan', 'Informatika'),
('L200160140', 'M. HAMMAM I', 'laki-laki', 'Informatika'),
('L200160147', 'DIANA GUSTI A', 'perempuan', 'Informatika'),
('L200160149', 'SUPRIADI', 'laki-laki', 'Informatika'),
('L200160159', 'RETNO ARUM', 'perempuan', 'Informatika'),
('L200160163', 'WINDA KUSUMA WARDHANI', 'perempuan', 'Informatika'),
('L200160164', 'DWI WAHYU KURNIAWAN', 'laki-laki', 'Informatika'),
('L200160165', 'LISA CHOLISATUN NURAINI', 'perempuan', 'Informatika'),
('L200160169', 'M. RASYID BAKHTIAR', 'laki-laki', 'Informatika'),
('L200160171', 'AISYAH AYU M', 'perempuan', 'Informatika'),
('L200160172', 'PUTERA ISLAMIYATI R', 'laki-laki', 'Informatika'),
('L200160177', 'NANDA YUSUF NUR PRATAMA', 'laki-laki', 'Informatika'),
('L200160178', 'APRILLIYA PUSPITA AJI', 'laki-laki', 'Informatika'),
('L200160179', 'WIDHAR DWIATMOKO', 'laki-laki', 'Informatika'),
('L200160180', 'INAYAH NUR FADHILAH', 'perempuan', 'Informatika'),
('L200160182', 'AYU PUTRI WARDHANI', 'perempuan', 'Informatika'),
('L200160183', 'MUHAMMAD MU''TASHIM BILLAH', 'laki-laki', 'Informatika'),
('L200160184', 'IKHSANAJI ISBAN PRATAMA', 'laki-laki', 'Informatika'),
('L200170025', 'MUHAMMAD IZZUDDIN', 'laki-laki', 'Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `nim` varchar(225) NOT NULL,
  `jml_visual` varchar(225) NOT NULL,
  `jml_auditorial` varchar(225) NOT NULL,
  `jml_readwrite` varchar(225) NOT NULL,
  `jml_kinestetik` varchar(225) NOT NULL,
  `kecenderungan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`nim`, `jml_visual`, `jml_auditorial`, `jml_readwrite`, `jml_kinestetik`, `kecenderungan`) VALUES
('L100140049', '3', '6', '4', '3', 'Auditorial'),
('L100140097', '3', '3', '4', '8', 'Kinestetik'),
('L100140107', '0', '6', '6', '4', 'Auditorial-Read/Write'),
('L100140110', '5', '5', '3', '3', 'Auditorial-Visual'),
('L100140112', '6', '5', '2', '5', 'Auditorial-Kinestetik-Visual'),
('L100140113', '3', '5', '3', '5', 'Auditorial-Kinestetik'),
('L100140129', '5', '4', '3', '4', 'Auditorial-Read/Write-Kinestetik-Visual'),
('L100144016', '4', '3', '1', '8', 'Kinestetik'),
('L100144018', '3', '7', '3', '10', 'Kinestetik'),
('L100144020', '5', '6', '3', '2', 'Auditorial-Visual'),
('L100150024', '4', '7', '6', '4', 'Auditorial-Read/Write'),
('L100150080', '1', '5', '2', '8', 'Kinestetik'),
('L100150083', '4', '9', '4', '4', 'Auditorial'),
('L100150099', '2', '6', '2', '6', 'Auditorial-Kinestetik'),
('L100150105', '1', '8', '1', '6', 'Auditorial'),
('L100150115', '3', '9', '7', '10', 'Auditorial-Kinestetik-Read/Write'),
('L100150117', '6', '9', '7', '10', 'Auditorial-Kinestetik-Read/Write'),
('L100150118', '2', '8', '4', '2', 'Auditorial'),
('L100150120', '2', '9', '2', '3', 'Auditorial'),
('L100150125', '4', '8', '1', '3', 'Auditorial'),
('L100150127', '10', '16', '9', '8', 'Auditorial'),
('L100150129', '1', '7', '6', '9', 'Auditorial-Kinestetik-Read/Write'),
('L100150132', '7', '6', '5', '6', 'Auditorial-Read/Write-Kinestetik-Visual'),
('L100150138', '1', '6', '5', '4', 'Auditorial-Read/Write'),
('L100150140', '2', '10', '2', '2', 'Auditorial'),
('L100150148', '5', '4', '4', '3', 'Auditorial-Read/Write-Kinestetik-Visual'),
('L100150152', '3', '3', '7', '5', 'Read/write'),
('L100160002', '2', '5', '5', '4', 'Auditorial-Kinestetik-Read/Write'),
('L100160007', '2', '2', '4', '8', 'Kinestetik'),
('L100160032', '3', '6', '3', '4', 'Auditorial'),
('L100160059', '6', '7', '2', '7', 'Auditorial-Kinestetik-Visual'),
('L100160088', '3', '6', '5', '6', 'Auditorial-Kinestetik-Read/Write'),
('L100160117', '4', '7', '4', '1', 'Auditorial'),
('L100160122', '2', '5', '5', '4', 'Auditorial-Kinestetik-Read/Write'),
('L100160123', '3', '8', '4', '1', 'Auditorial'),
('L100160124', '5', '3', '3', '5', 'Kinestetik-Visual'),
('L100160125', '0', '5', '3', '8', 'Kinestetik'),
('L100160137', '2', '5', '1', '8', 'Kinestetik'),
('L100160142', '1', '9', '1', '5', 'Auditorial'),
('L100160167', '3', '5', '2', '6', 'Auditorial-Kinestetik'),
('L100170006', '4', '5', '1', '6', 'Auditorial-Kinestetik-Visual'),
('L100170019', '9', '9', '6', '11', 'Auditorial-Read/Write-Kinestetik-Visual'),
('L100170029', '2', '2', '0', '12', 'Kinestetik'),
('L100170030', '1', '3', '6', '9', 'Kinestetik'),
('L100170048', '5', '12', '3', '6', 'Auditorial'),
('L100174024', '4', '7', '2', '3', 'Auditorial'),
('L100174112', '2', '7', '3', '4', 'Auditorial'),
('L101173007', '2', '8', '3', '3', 'Auditorial'),
('L101173009', '0', '5', '3', '8', 'Kinestetik'),
('L200140002', '4', '5', '7', '10', 'Kinestetik'),
('L200140008', '9', '11', '13', '16', 'Kinestetik-Read/Write'),
('L200140100', '1', '8', '4', '3', 'Auditorial'),
('L200150093', '8', '8', '6', '5', 'Auditorial-Read/Write-Visual'),
('L200150117', '5', '11', '6', '11', 'Auditorial-Kinestetik'),
('L200150123', '5', '12', '5', '10', 'Auditorial-Kinestetik'),
('L200154010', '6', '9', '10', '10', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160007', '7', '7', '12', '8', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160008', '2', '4', '4', '7', 'Kinestetik'),
('L200160009', '3', '5', '8', '4', 'Read/Write'),
('L200160010', '6', '10', '6', '9', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160013', '5', '7', '5', '6', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160014', '1', '8', '4', '3', 'Auditorial'),
('L200160015', '7', '7', '4', '6', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160017', '4', '13', '8', '9', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160018', '2', '9', '8', '11', 'Auditorial-Kinestetik-Read/Write'),
('L200160022', '2', '8', '2', '4', 'Auditorial'),
('L200160024', '1', '4', '5', '6', 'Auditorial-Kinestetik-Read/Write'),
('L200160031', '4', '7', '3', '8', 'Auditorial-Kinestetik'),
('L200160039', '3', '6', '6', '5', 'Auditorial-Kinestetik-Read/Write'),
('L200160079', '5', '7', '6', '9', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160082', '6', '9', '5', '10', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160105', '4', '7', '2', '8', 'Auditorial-Kinestetik'),
('L200160106', '11', '9', '5', '8', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160107', '9', '8', '8', '4', 'Auditorial-Read/Write-Visual'),
('L200160112', '6', '5', '8', '4', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160113', '4', '9', '1', '4', 'Auditorial'),
('L200160115', '3', '7', '7', '7', 'Auditorial-Kinestetik-Read/Write'),
('L200160119', '3', '8', '6', '5', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160120', '3', '9', '4', '9', 'Auditorial-Kinestetik'),
('L200160122', '2', '5', '3', '9', 'Kinestetik'),
('L200160125', '5', '9', '7', '10', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160126', '4', '3', '1', '8', 'Kinestetik'),
('L200160128', '9', '8', '5', '14', 'Kinestetik'),
('L200160140', '13', '9', '6', '3', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160147', '7', '9', '9', '5', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160149', '2', '6', '5', '3', 'Auditorial-Read/Write'),
('L200160159', '1', '10', '1', '4', 'Auditorial'),
('L200160163', '2', '5', '4', '5', 'Auditorial-Kinestetik-Read/Write'),
('L200160164', '6', '4', '5', '8', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160165', '2', '4', '3', '7', 'Kinestetik'),
('L200160169', '7', '9', '8', '1', 'Auditorial-Read/Write-Visual'),
('L200160171', '3', '3', '0', '10', 'Kinestetik'),
('L200160172', '10', '7', '9', '8', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160177', '3', '10', '4', '7', 'Auditorial'),
('L200160178', '8', '6', '8', '8', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160179', '5', '8', '10', '10', 'Auditorial-Kinestetik-Read/Write-Visual'),
('L200160180', '5', '6', '6', '3', 'Auditorial-Read/Write-Visual'),
('L200160182', '2', '6', '3', '5', 'Auditorial-Kinestetik'),
('L200160183', '6', '7', '6', '13', 'Kinestetik'),
('L200160184', '1', '10', '3', '7', 'Auditorial'),
('L200170025', '10', '10', '13', '8', 'Auditorial-Kinestetik-Read/Write-Visual');

-- --------------------------------------------------------

--
-- Table structure for table `modalitas`
--

CREATE TABLE `modalitas` (
  `id_modalitas` int(11) NOT NULL,
  `id_pertanyaan` varchar(225) NOT NULL,
  `jns_modalitas` varchar(225) NOT NULL,
  `des_modalitas` varchar(50000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modalitas`
--

INSERT INTO `modalitas` (`id_modalitas`, `id_pertanyaan`, `jns_modalitas`, `des_modalitas`) VALUES
(1, 'Q01', 'Visual', 'Melihat ide di internet atau di beberapa buku memasak dari gambar-gambarnya '),
(2, 'Q02', 'Visual', 'Mengikuti diagram-diagram yang sudah ada dalam buku'),
(3, 'Q03', 'Visual', 'Diagram, peta, dan grafik sebagai petunjuk visual'),
(4, 'Q04', 'Visual', 'Membuat diagram atau grafik untuk membantu menjelaskan sesuatu'),
(5, 'Q05', 'Visual', 'Gunakan peta untuk menunjukkan lokasinya pada mereka'),
(6, 'Q06', 'Visual', 'Sebuah desain yang modern dan tampilannya yang bagus'),
(7, 'Q07', 'Visual', 'Menggambar, atau menunjukkan padanya sebuah peta, atau memberinya peta'),
(8, 'Q08', 'Visual', 'Melihat diagram'),
(9, 'Q09', 'Visual', 'Desain yang menarik dan fitur-fitur visual'),
(10, 'Q10', 'Visual', 'Menggunakan grafik untuk menunjukkan apa yang telah anda capai'),
(11, 'Q11', 'Visual', 'Menunjukkan peta dan gambar di internet pada mereka'),
(12, 'Q12', 'Visual', 'Diagram atau grafik '),
(13, 'Q13', 'Visual', 'Melihat apa yang orang lain sedang makan atau melihat gambar setiap sajian'),
(14, 'Q14', 'Visual', 'Bagaimana menariknya tampilan buku tersebut'),
(15, 'Q15', 'Visual', 'Menunjukkan pada anda daiagram tentang apa yang terjadi'),
(16, 'Q16', 'Visual', 'Diagram yang menunjukkan kamera dan setiap bagiannya digunakan'),
(17, 'Q01', 'Auditorial', 'Bertanya pada teman untuk masukan'),
(18, 'Q02', 'Auditorial', 'Bicara pada orang yang tahu tentang program'),
(19, 'Q03', 'Auditorial', 'Mendengarkan seseorang menjelaskannya dan mengajukan pertanyaan'),
(20, 'Q04', 'Auditorial', 'Menulis beberapa kata kunci dan berlatih mengucapkan pidato anda lagi dan lagi'),
(21, 'Q05', 'Auditorial', 'Ponsel, teks atau mengirim email kepada mereka'),
(22, 'Q06', 'Auditorial', 'Penjual yang mengatakan pada saya tentang fitur-fiturnya'),
(23, 'Q07', 'Auditorial', 'Mengatakan padanya kemana arahnya '),
(24, 'Q08', 'Auditorial', 'Mendengarkan'),
(25, 'Q09', 'Auditorial', 'Audio, saluran dimana saya bisa mendengarkan musik, program radio atau wawancara-wawancara'),
(26, 'Q10', 'Auditorial', 'Dari seseorang yang membicarakan tentang itu dengan anda'),
(27, 'Q11', 'Auditorial', 'Berbicara atau mengatur pembicaraan mereka tentang taman atau suaka margasatwa'),
(28, 'Q12', 'Auditorial', 'Tanya jawab, berbicara, diskusi kelompok, atau bintang tamu pembicara'),
(29, 'Q13', 'Auditorial', 'Mendengarkan pelayan atau bertanya pada teman untuk merekomendasikan pilihan'),
(30, 'Q14', 'Auditorial', 'Seorang teman membicarakannya dan merekomendasikannya'),
(31, 'Q15', 'Auditorial', 'Mendeskripsikan apa yang terjadi '),
(32, 'Q16', 'Auditorial', 'Sebuah kesempatan untuk tanya jawab tentang kamera dan fitur-fiturnya'),
(33, 'Q01', 'Read-Write', 'Melihat sebuah buku memasak dimana anda tahu disana ada resep bagus'),
(34, 'Q02', 'Read-Write', 'Membaca instruksi tertulis yang ada pada program'),
(35, 'Q03', 'Read-Write', 'Instruksi tertulis seperti buku panduan '),
(36, 'Q04', 'Read-Write', 'Menulis pidato anda dan belajar dari membacanya selama beberapa waktu'),
(37, 'Q05', 'Read-Write', 'Beri mereka salinan jadwal cetak'),
(38, 'Q06', 'Read-Write', 'Membaca detail atau memerikasa fitur-fitur secara online'),
(39, 'Q07', 'Read-Write', 'Menuliskan arahnya'),
(40, 'Q08', 'Read-Write', 'Membaca kata-kata deskripsi'),
(41, 'Q09', 'Read-Write', 'Tulisan deskripsi menarik, daftar dan penjelasan'),
(42, 'Q10', 'Read-Write', 'Menggunakan deskripsi tertulis tentang pencapaian anda'),
(43, 'Q11', 'Read-Write', 'Memberi mereka buku atau pamphlet tentang taman atau suaka margasatwa'),
(44, 'Q12', 'Read-Write', 'Handout, buku, atau membaca'),
(45, 'Q13', 'Read-Write', 'Memilih berdasarkan deskripsi di menu'),
(46, 'Q14', 'Read-Write', 'Dengan cepat membaca bagian pada buku tersebut'),
(47, 'Q15', 'Read-Write', 'Memberi anda sesuatu untuk dibaca untuk menjelaskan apa yang sedang terjadi'),
(48, 'Q16', 'Read-Write', 'Instruksi tertulis yang jelas disertai daftar dan poin-poin tentang apa yang harus dilakukan'),
(49, 'Q01', 'Kinestetik', 'Memasak sesuatu yang anda tahu tanpa membutuhkan panduan'),
(50, 'Q02', 'Kinestetik', 'Menggunakan kontrol atau keyboard'),
(51, 'Q03', 'Kinestetik', 'Melihat sebuah peragaan atau demonstrasi'),
(52, 'Q04', 'Kinestetik', 'Menggabungkan banyak contoh dan cerita untuk membuat pembicaraan itu nyata dan praktis'),
(53, 'Q05', 'Kinestetik', 'Mendeskripsikan beberapa gambaran yang akan mereka alami'),
(54, 'Q06', 'Kinestetik', 'Mencoba atau menguji barang tersebut'),
(55, 'Q07', 'Kinestetik', 'Pergi bersamanya'),
(56, 'Q08', 'Kinestetik', 'Melihat apa yang dilakukan'),
(57, 'Q09', 'Kinestetik', 'Sesuatu yang bisa saya klik, geser atau coba'),
(58, 'Q10', 'Kinestetik', 'Menggunakan contoh dari apa yang telah anda lakukan'),
(59, 'Q11', 'Kinestetik', 'Membawa mereka ke taman atau suaka margasatwa dan berjalan mendampingi mereka'),
(60, 'Q12', 'Kinestetik', 'Peragaan, model atau sesi praktek'),
(61, 'Q13', 'Kinestetik', 'Memilih sesuatu yang telah anda miliki sebelumnya'),
(62, 'Q14', 'Kinestetik', 'Buku tersebut memiliki cerita kisah nyata, pengalaman dan permisalan'),
(63, 'Q15', 'Kinestetik', 'Menggunakan model plastik untuk menunjukkan apa yang sedang terjadi'),
(64, 'Q16', 'Kinestetik', 'Banyak contoh dari bagus dan tidaknya foto dan bagaimana cara untuk meningkatkan kualitas foto tersebut');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id_pertanyaan` varchar(225) NOT NULL,
  `des_pertanyaan` varchar(50000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id_pertanyaan`, `des_pertanyaan`) VALUES
('Q01', 'Anda akan memasak sesuatu untuk suguhan istimewa. Anda akan melakukannya dengan: '),
('Q02', 'Anda ingin belajar sebuah program baru, skill atau game di komputer. Anda akan melakukannya dengan:'),
('Q03', 'Ingatlah ketika anda belajar melakukan hal baru (kecuali belajar skill fisik, contoh: belajar mengendarai sepeda) . Anda akan belajar baik dengan:'),
('Q04', 'Anda harus membuat sebuah pidato penting di sebuah konferensi atau acara khusus. Anda akan melakukannya dengan:'),
('Q05', 'Anda merencanakan liburan dengan teman teman anda. Anda menginginkan tanggapan dari teman teman anda tentang rencana anda. Anda akan melakukannya dengan:'),
('Q06', 'Anda akan membeli kamera digital atau ponsel. Selain harga, apa yang paling mempengaruhi keputusan anda?'),
('Q07', 'Anda membantu seseorang yang ingin pergi kebandara, di pusat kota atau stasiun kereta api. Anda akan melakukannya dengan:'),
('Q08', 'Sebuah website memiliki vidio yang menunjukkan bagaimana cara membuat sebuah grafik special. Ada seorang yang berbicara, beberapa daftar dan kata-kata yang mendeskripsikan apa yang harus dilakukan dan beberapa diagram. Anda belajar paling banyak dari:'),
('Q09', 'Saya menyukai website yang mempunyai:'),
('Q10', 'Anda telah menyelesaikan sebuah kompetisi atau ujian dan menginginkan beberapa tanggapan dari orang lain. Anda ingin tanggapan didapat dengan :'),
('Q11', 'Sekelompok turis ingin mempelajari taman atau suaka margasatwa di daerah anda. Yang anda lakukan:'),
('Q12', 'Anda lebih memilih seorang guru atau presenter yang menggunakan:'),
('Q13', 'Anda akan memilih makanan di restoran atau kafe. Yang anda lakukan adalah:'),
('Q14', 'Selain harga, apa yang paling mempengaruhi keputusan anda untuk membeli buku non-fiksi baru?'),
('Q15', 'Anda memiliki masalah dengan jantung anda. Anda lebih suka dokter:'),
('Q16', 'Anda menggunakan buku, CD atau website untuk mempelajari cara memotret dengan kamera digital baru anda. Yang anda inginkan dalam penjelasannya adalah:');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id_profil` int(255) NOT NULL,
  `jns_modalitas` varchar(255) NOT NULL,
  `karakter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id_profil`, `jns_modalitas`, `karakter`) VALUES
(1, 'Modalitas Visual', 'Belajar lebih maksimal dengan melihat materi edukasi visual seperti diagram, angka-angka, serta gambar yang disertai dengan penjelasan'),
(2, 'Modalitas Auditorial', 'Belajar dengan maksimal melalui mendengar dan penyampaian secara verbal seperti mendengarkan penjelasan pengajar di depan kelas'),
(3, 'Modalitas Read/Write', 'Belajar lebih baik melalui membaca dan menulis, seperti menulis catatan dari materi yang di jelaskan pengajar, membaca materi bacaan atau materi cetak'),
(4, 'Modalitas Kinestetik', 'Melakukan praktek langsung atau latihan dengan eksperimen serta manipulasi objek dengan proses fisik');

-- --------------------------------------------------------

--
-- Table structure for table `solusi`
--

CREATE TABLE `solusi` (
  `id_solusi` varchar(100) NOT NULL,
  `jns_modalitas` varchar(225) NOT NULL,
  `intake` text NOT NULL,
  `output` text NOT NULL,
  `swot` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `solusi`
--

INSERT INTO `solusi` (`id_solusi`, `jns_modalitas`, `intake`, `output`, `swot`) VALUES
('1', 'Visual', 'Senantiasa melihat bibir guru yang sedang mengajar; Menyukai instruksi tertulis, foto dan ilustrasi untuk dilihat; Saat petunjuk untuk melakukan sesuatu diberikan biasanya akan melihat teman-teman lainnya baru dia sendiri yang bertindak; Biasanya tidak dapat mengingat informasi yang diberikan secara lisan; Menyukai diagram, kalender maupun grafik time-line untuk mengingat\r\nbagian peristiwa;', 'Gaya belajar visual  (visual  learner)  menitikberatkan ketajaman penglihatan, artinya, bukti-bukti konkret harus diperlihatkan terlebih dahulu agar siswa paham. Kecenderungan ini mencakup menggambarkan informasi dalam bentuk peta,diagram, grafik, flow chart dan simbol visual seperti panah, lingkaran, hirarki dan materi lain yang  digunakan  instruktur  untuk mempresentasikan  hal-hal  yang  dapat disampaikan dalam kata-kata. Ketika memilih suatu benda Anda lebih memperhatikan tampilan benda tersebut. Anda juga tertarik dengan warna, tata letak dan desain. Cenderung menggunakan gerakan tubuh untuk mengekspresikan atau mengganti sebuah kata saat mengungkapkan sesuatu; Kurang menyukai berbicara di depan kelompok dan kurang menyukai untuk mendengarkan orang lain.', 'Mempelajari materi dengan membaca catatan dan membuat ringkasan. Mengorganisir materi belajarnya dengan hati-hati; Media gambar, video, poster dan sebagainya; Buku yang banyak mencantumkan diagram atau gambar; Flow chart; Grafik; Menandai bagian-bagian yang penting dari bahan ajar dengan menggunakan warna yang berbeda;\r\nserta Symbol-simbol visual. Mengganti kata-kata dengan symbol atau gambar.'),
('2', 'Kinestetik', 'Untuk mengambil informasi:  semua indera Anda - penglihatan, sentuhan, rasa, bau, pendengaran dilibatkan. Mengingat informasi lebih mudah dengan praktik belajar di laboratorium, melakukan Karyawisata wisata, studi lapangan. Contoh prinsip dosen yang memberikan informasi melalui objek nyata seperti aplikasi/praktek pendekatan langsung (komputasi) trial and error yang akan dengan mudah mendefinisikan serta menemukan solusi untuk masalah. Mengingat secara baik bila secara fisik terlibat aktif dalam proses pembelajaran', 'Suka menyentuh segala sesuatu yang\r\ndijumpainya; Sulit untuk berdiam\r\ndiri; Suka mengerjakan segala sesuatu\r\ndengan menggunakan tangan;\r\nBiasanya memiliki koordinasi tubuh\r\nyang baik. Mengungkapkan minat dan\r\nketertarikan terhadap sesuatu secara\r\nfisik dengan bekerja secara antusias.\r\n\r\n', 'Strategi belajarnya meliputi:\r\nMelalui Mengingat kejadian nyata yang terjadi; Masukan berbagai macam\r\ncontoh untuk memudahkan dalam mengingat konsep; Gunakan benda-benda untuk mengilustrasikan ide; Kembali ke laoratorium atau tempat belajar dapat melakukan eksperimen; serta Mengingat kembali mengenai eksperimen, kunjungan lapangan dan sebagainya.'),
('3', 'Auditorial', 'Mampu\r\nmengingat dengan baik apa yang\r\nmereka katakana maupun yang orang\r\nlain sampaikan; Mengingat dengan\r\nbaik dengan jalan selalu\r\nmengucapkan dengan nada keras dan\r\nmengulang-ulang kalimat; Sangat\r\nmenyukai diskusi kelompok;\r\nMenyukai diskusi yang lebih lama\r\nterutama untuk hal-hal yang kurang\r\nmereka pahami; Mampu menginngat\r\ndengan baik materi yang didiskusikan\r\ndalam kelompok atau kelas;\r\nMengenal banyak sekali lagu atau\r\niklan TV dan bahkan dapat\r\nmenirukannya secara tepat dan\r\nkomplit; Suka berbicara; Kurang suka\r\ntugas membaca (dan pada umumnya\r\nbukanlah pembaca yang baik);\r\nKurang dapat mengingat dengan baik\r\napa yang baru saja dibacanya; Kurang\r\ndalam mengerjakan tugas mengarang\r\natau menulis. Sukar bekerja dengan\r\ntenang tanpa menimbulkan suara;\r\nserta Mudah terganggu konsentrasi\r\nkarena suara dan juga susah\r\nberkonsentrasi bila tidak ada suara\r\nsama sekali.', 'Menghadiri kelas; Diskusi; Membahas suatu topik bersama dengan teman; Membahas suatu topic bersama dengan guru; Menjelaskan ide-ide baru kepada orang lain; Menggunakan perekam; Mengingat cerita, contoh atau lelucon yang menarik; Menjelaskan bahan yang didapat secara visual (gambar, power\r\npoint dsb)', 'Strategi belajar meliputi: Catatan\r\nyang dibuat mungkin sangat tidak\r\nmemadai. Tambahkan informasi yang\r\ndidapat dengan cara berbicara dengan\r\norang lain dan mengumpulkan catatan\r\ndari buku; Rekam ringkasan dari\r\ncatatn yang dibuat dan dengarkan\r\nrekaman tersebut; Minta orang lain\r\nuntuk mendengar pemahaman yang\r\nditerima mengenai suatu topic; serta\r\nBaca buku atau catatan dengan keras.'),
('4', 'Read/Write', 'Mahasiswa dengan modalitas read-write cenderung menerima informasi melalui membaca daftar, judul, kamus, glosari, definisi, handout, buku teks, bacaan - perpustakaan, catatan, guru yang menggunakan kata-kata dengan baik dan memiliki banyak informasi dalam kalimat dan catatan, esai, manual/modul (komputer dan laboratorium).\r\n', 'Anda menyukai bacaan karena tertekannya pada kata-kata dan daftar yang terkandung didalamnya karena anda percaya maknanya ada di dalam kata-kata tersebut. Menulis kembali jawaban dari latihan yang anda lakukan, berlatih dengan pertanyaan pilihan ganda, tuliskan paragraf, permulaan dan akhiran. Atur kata-kata Anda ke dalam hierarki dan poin poin. ', 'Strategi belajarnya meliputi: Tuliskan kata-kata secara berulang-ulang; Baca\r\ncatatan Anda (dengan sunyi) secara berkali-kali; Tulis kembali ide atau informasi dengan kalimat yang berbeda; dan Terjemahkan semua diagram, gambar, dan sebagainya ke dalam kata-kata');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminku`
--
ALTER TABLE `adminku`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `biodata_pengunjung`
--
ALTER TABLE `biodata_pengunjung`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `modalitas`
--
ALTER TABLE `modalitas`
  ADD PRIMARY KEY (`id_modalitas`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indexes for table `solusi`
--
ALTER TABLE `solusi`
  ADD PRIMARY KEY (`id_solusi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminku`
--
ALTER TABLE `adminku`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modalitas`
--
ALTER TABLE `modalitas`
  MODIFY `id_modalitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id_profil` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
