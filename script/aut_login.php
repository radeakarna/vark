<?php
session_start();
include "koneksi.php"; //Establishing connection with our database
$error = ""; //Variable for storing our errors.
if (isset($_POST["submit"])) {
    if (empty($_POST["username"]) || empty($_POST["password"])) {
        $error = "username dan password tidak boleh kosong";
        echo "<script>alert('$error');window.history.go(-1);</script>";

    } else {
        // Define $username and $password
        $username = $_POST['username'];
        $password = $_POST['password'];

        // To protect from MySQL injection
        $username = stripslashes($username);
        $password = stripslashes($password);
        $username = mysqli_real_escape_string($con, $username);
        $password = mysqli_real_escape_string($con, $password);
        $password = md5($password);

        //Check username and password from database
        $sql    = "SELECT *FROM adminku WHERE username='$username' and password='$password'";
        $result = mysqli_query($con, $sql);
        $row    = mysqli_fetch_array($result, MYSQLI_ASSOC);

        //If username and password exist in our database then create a session.
        //Otherwise echo error.
        if (mysqli_num_rows($result) == 1) {
            $_SESSION['username'] = $row['username'];
            $_SESSION['id_admin'] = $row['id_admin'];
            $_SESSION['status']   = 'login'; // Initializing Session
            header("location: ../adminku/index.php"); // Redirecting To Other Page
        } else {
            $error = "Incorrect username or password.";
            echo "<script>alert('Gagal Login');window.history.go(-1);</script>";
        }

    }
}
